-- used in re-running permissions 
DECLARE @RoleId nvarchar(1000)
DECLARE @adminId nvarchar(max) = (select Top(1) Id From DigitalAdmins);


--Rerun Dev_Allow_All permissions 
--Create 'Allow All' Role
SELECT @RoleId = Id FROM DigitalRoles WHERE Name = 'Dev_Allow_All'

--Assign all feaure permissions to the role
INSERT INTO DigitalRolePermissions (InsertAt, UpdateAt, DigitalRoleId, FeaturePermissionId, Allowed)
SELECT getdate(), getdate(), @RoleId, Id, 1 FROM FeaturePermissions
WHERE (Id NOT IN (SELECT FeaturePermissionId FROM DigitalRolePermissions WHERE DigitalRoleId = @RoleId)) AND (IsAdmin <> 1)

--Assign all digital users to the role
INSERT INTO DigitalUserRoles(InsertAt, UpdateAt, DigitalRoleId, DigitalUserId, AssignedBy)
SELECT getdate(), getdate(), @RoleId, Id, @adminId FROM DigitalUsers
WHERE Id NOT IN (SELECT DigitalUserId FROM DigitalUserRoles WHERE DigitalRoleId = @RoleId)


--
UPDATE ProductDefinitions
SET ExternalIdentifiers = '["ST*"]'
WHERE Id = 1;
UPDATE ProductDefinitions
SET ExternalIdentifiers = '["LT*"]'
WHERE Id = 2;