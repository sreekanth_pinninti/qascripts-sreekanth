BEGIN TRAN
insert into ProductFeatureContextLinks (insertat, updateat, ProductDefinitionId, FeatureProductContextId, Allowed)
select getdate(), getdate(), p.id, pc.id, 1
from productdefinitions as p 
cross join featureproductcontexts as pc
where not exists (select * from ProductFeatureContextLinks where ProductDefinitionId = p.id and FeatureProductContextId = pc.id)
COMMIT TRAN


---------------------------------------------------------------
-- Remove features from account types that should not have them
---------------------------------------------------------------
-- Deposit accounts should not allow incoming payments
BEGIN TRAN
DELETE FROM ProductFeatureContextLinks 
WHERE 
   ProductDefinitionId IN (SELECT Id from ProductDefinitions WHERE SubType = 'Deposit')
   AND FeatureProductContextId IN (SELECT Id FROM featureproductcontexts WHERE FullyQualifiedName LIKE 'Payments%To')

COMMIT TRAN


-- Credit Card accounts should not have Transfers
BEGIN TRAN
DELETE FROM ProductFeatureContextLinks 
WHERE 
   ProductDefinitionId IN (SELECT Id from ProductDefinitions WHERE SubType = 'Card')
   AND FeatureProductContextId IN (SELECT Id FROM featureproductcontexts WHERE FullyQualifiedName LIKE 'Transfer%')

COMMIT TRAN

-- Loan accounts should not have Transfers
BEGIN TRAN
DELETE FROM ProductFeatureContextLinks 
WHERE 
   ProductDefinitionId IN (SELECT Id from ProductDefinitions WHERE SubType = 'Loan')
   AND FeatureProductContextId IN (SELECT Id FROM featureproductcontexts WHERE FullyQualifiedName LIKE 'Transfer%')

COMMIT TRAN




-- Update DefaultShare w/ Metadata Group
DECLARE @AvailableMetadataDefinitionsId BIGINT

SET @AvailableMetadataDefinitionsId = NULL
select @AvailableMetadataDefinitionsId = id from MetadataDefinitionGroups where Name = 'Checking'

BEGIN TRAN
if exists (select * from ProductDefinitions with (updlock,serializable) where DefaultName = 'DefaultShare')
begin
   update ProductDefinitions set UpdateAt = GetDate(), AvailableMetadataDefinitionsId = @AvailableMetadataDefinitionsId
   where DefaultName = 'DefaultShare'
end
COMMIT TRAN

-- Update DefaultLoan w/ Metadata Group

set @AvailableMetadataDefinitionsId = NULL
select @AvailableMetadataDefinitionsId = id from MetadataDefinitionGroups where Name = 'PersonalLoan'
BEGIN TRAN
if exists (select * from ProductDefinitions with (updlock,serializable) where DefaultName = 'DefaultLoan')
begin
   update ProductDefinitions set UpdateAt = GetDate(), AvailableMetadataDefinitionsId = @AvailableMetadataDefinitionsId
   where DefaultName = 'DefaultLoan'
end
COMMIT TRAN
