
DECLARE @FeatureId bigint
DECLARE @MetadataDefinitionGroupId bigint


SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Accounts')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Account Features', DisplayName = 'Account Features', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Accounts'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Accounts', 'Account Features', 'Account Features', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Accounts'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update account settings', Tag = '', FullyQualifiedName = 'Account_UpdateSettings', IsAdmin = 0
   WHERE Name = 'UpdateSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateSettings', 'Update account settings', 'Account_UpdateSettings', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateSortOrder' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update account sort order', Tag = '', FullyQualifiedName = 'Account_UpdateSortOrder', IsAdmin = 0
   WHERE Name = 'UpdateSortOrder' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateSortOrder', 'Update account sort order', 'Account_UpdateSortOrder', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateFavorite' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update account favorite', Tag = '', FullyQualifiedName = 'Account_UpdateFavorite', IsAdmin = 0
   WHERE Name = 'UpdateFavorite' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateFavorite', 'Update account favorite', 'Account_UpdateFavorite', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access account settings', Tag = '', FullyQualifiedName = 'AccountSettings_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access account settings', 'AccountSettings_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountSettings_EnableEStatements'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'EnableEStatements' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for e-Statement', Tag = '', FullyQualifiedName = 'AccountSettings_EnableEStatements', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'EnableEStatements' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EnableEStatements', 'Valid Product for e-Statement', 'AccountSettings_EnableEStatements', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountSettings_CheckOverdraft'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'CheckOverdraft' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Overdraft Protection', Tag = '', FullyQualifiedName = 'AccountSettings_CheckOverdraft', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'CheckOverdraft' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CheckOverdraft', 'Valid Product for Overdraft Protection', 'AccountSettings_CheckOverdraft', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountSettings_DebitOverdraft'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'DebitOverdraft' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Debit Card Overdraft protection', Tag = '', FullyQualifiedName = 'AccountSettings_DebitOverdraft', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'DebitOverdraft' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DebitOverdraft', 'Valid Product for Debit Card Overdraft protection', 'AccountSettings_DebitOverdraft', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'AccountSummary')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Account Summary', DisplayName = 'Account Summary', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'AccountSummary'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'AccountSummary', 'Account Summary', 'Account Summary', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'AccountSummary'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access account summary', Tag = '', FullyQualifiedName = 'AccountSummary_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access account summary', 'AccountSummary_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountSummary_View'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'View' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Account Summary Display', Tag = '', FullyQualifiedName = 'AccountSummary_View', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'View' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'View', 'Valid Product for Account Summary Display', 'AccountSummary_View', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Authentication')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Authentication', DisplayName = 'Authentication', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Authentication'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Authentication', 'Authentication', 'Authentication', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Authentication'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Billpay')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Billpay', DisplayName = 'Billpay', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Billpay'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Billpay', 'Billpay', 'Billpay', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Billpay'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Bill Pay', Tag = '', FullyQualifiedName = 'Billpay_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Bill Pay', 'Billpay_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddCompanyBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add a company biller', Tag = '', FullyQualifiedName = 'Billpay_AddCompanyBiller', IsAdmin = 0
   WHERE Name = 'AddCompanyBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddCompanyBiller', 'Add a company biller', 'Billpay_AddCompanyBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditCompanyBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit company biller', Tag = '', FullyQualifiedName = 'Billpay_EditCompanyBiller', IsAdmin = 0
   WHERE Name = 'EditCompanyBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditCompanyBiller', 'Edit company biller', 'Billpay_EditCompanyBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteCompanyBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete company biller', Tag = '', FullyQualifiedName = 'Billpay_DeleteCompanyBiller', IsAdmin = 0
   WHERE Name = 'DeleteCompanyBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteCompanyBiller', 'Delete company biller', 'Billpay_DeleteCompanyBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddIndividualBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add an individual biller', Tag = '', FullyQualifiedName = 'Billpay_AddIndividualBiller', IsAdmin = 0
   WHERE Name = 'AddIndividualBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddIndividualBiller', 'Add an individual biller', 'Billpay_AddIndividualBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditIndividualBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit individual biller', Tag = '', FullyQualifiedName = 'Billpay_EditIndividualBiller', IsAdmin = 0
   WHERE Name = 'EditIndividualBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditIndividualBiller', 'Edit individual biller', 'Billpay_EditIndividualBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteIndividualBiller' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete individual biller', Tag = '', FullyQualifiedName = 'Billpay_DeleteIndividualBiller', IsAdmin = 0
   WHERE Name = 'DeleteIndividualBiller' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteIndividualBiller', 'Delete individual biller', 'Billpay_DeleteIndividualBiller', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'CreateCompanyPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate company payment', Tag = '', FullyQualifiedName = 'Billpay_CreateCompanyPayment', IsAdmin = 0
   WHERE Name = 'CreateCompanyPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CreateCompanyPayment', 'Initiate company payment', 'Billpay_CreateCompanyPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditCompanyPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit scheduled company payment', Tag = '', FullyQualifiedName = 'Billpay_EditCompanyPayment', IsAdmin = 0
   WHERE Name = 'EditCompanyPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditCompanyPayment', 'Edit scheduled company payment', 'Billpay_EditCompanyPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteCompanyPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete scheduled company payment', Tag = '', FullyQualifiedName = 'Billpay_DeleteCompanyPayment', IsAdmin = 0
   WHERE Name = 'DeleteCompanyPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteCompanyPayment', 'Delete scheduled company payment', 'Billpay_DeleteCompanyPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'CreateIndividualPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate individual payment ', Tag = '', FullyQualifiedName = 'Billpay_CreateIndividualPayment', IsAdmin = 0
   WHERE Name = 'CreateIndividualPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CreateIndividualPayment', 'Initiate individual payment ', 'Billpay_CreateIndividualPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditIndividualPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit scheduled individual payment', Tag = '', FullyQualifiedName = 'Billpay_EditIndividualPayment', IsAdmin = 0
   WHERE Name = 'EditIndividualPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditIndividualPayment', 'Edit scheduled individual payment', 'Billpay_EditIndividualPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteIndividualPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete scheduled individual payment', Tag = '', FullyQualifiedName = 'Billpay_DeleteIndividualPayment', IsAdmin = 0
   WHERE Name = 'DeleteIndividualPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteIndividualPayment', 'Delete scheduled individual payment', 'Billpay_DeleteIndividualPayment', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditContactInformation' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update Bill Pay Contact Information', Tag = '', FullyQualifiedName = 'Billpay_EditContactInformation', IsAdmin = 0
   WHERE Name = 'EditContactInformation' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditContactInformation', 'Update Bill Pay Contact Information', 'Billpay_EditContactInformation', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Register' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Register as a new Bill Pay user', Tag = '', FullyQualifiedName = 'Billpay_Register', IsAdmin = 0
   WHERE Name = 'Register' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Register', 'Register as a new Bill Pay user', 'Billpay_Register', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'BillPay_Source'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Source' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Bill Payment source', Tag = '', FullyQualifiedName = 'BillPay_Source', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Source' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Source', 'Valid Product for Bill Payment source', 'BillPay_Source', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Cards')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Card Management', DisplayName = 'Card Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Cards'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Cards', 'Card Management', 'Card Management', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Cards'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Card Management', Tag = '', FullyQualifiedName = 'Cards_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Card Management', 'Cards_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateTravelNotice' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update travel notice', Tag = '', FullyQualifiedName = 'Cards_UpdateTravelNotice', IsAdmin = 0
   WHERE Name = 'UpdateTravelNotice' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateTravelNotice', 'Update travel notice', 'Cards_UpdateTravelNotice', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ChangeCardPin' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update PIN', Tag = '', FullyQualifiedName = 'Cards_ChangeCardPin', IsAdmin = 0
   WHERE Name = 'ChangeCardPin' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ChangeCardPin', 'Update PIN', 'Cards_ChangeCardPin', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ChangeCardStatus' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Enable/disable card(s)', Tag = '', FullyQualifiedName = 'Cards_ChangeCardStatus', IsAdmin = 0
   WHERE Name = 'ChangeCardStatus' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ChangeCardStatus', 'Enable/disable card(s)', 'Cards_ChangeCardStatus', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AccessExternalSite' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access an external site for card payments and management via SSO', Tag = '', FullyQualifiedName = 'Cards_AccessExternalSite', IsAdmin = 0
   WHERE Name = 'AccessExternalSite' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AccessExternalSite', 'Access an external site for card payments and management via SSO', 'Cards_AccessExternalSite', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Cards_SSO'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'SSO' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for SSO to External Provider', Tag = '', FullyQualifiedName = 'Cards_SSO', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'SSO' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'SSO', 'Valid Product for SSO to External Provider', 'Cards_SSO', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'CheckImages')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Check Images', DisplayName = 'Check Images', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'CheckImages'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'CheckImages', 'Check Images', 'Check Images', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'CheckImages'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access check images', Tag = '', FullyQualifiedName = 'CheckImages_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access check images', 'CheckImages_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'CheckOrder')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Check Order', DisplayName = 'Check Order', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'CheckOrder'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'CheckOrder', 'Check Order', 'Check Order', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'CheckOrder'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access check order', Tag = '', FullyQualifiedName = 'CheckOrder_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access check order', 'CheckOrder_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'CheckOrder_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Account for Check Reorder', Tag = '', FullyQualifiedName = 'CheckOrder_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid Account for Check Reorder', 'CheckOrder_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Deposits')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Mobile Deposit', DisplayName = 'Mobile Deposit', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Deposits'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Deposits', 'Mobile Deposit', 'Mobile Deposit', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Deposits'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access mobile deposit', Tag = '', FullyQualifiedName = 'Deposits_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access mobile deposit', 'Deposits_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Deposits_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Mobile Deposit', Tag = '', FullyQualifiedName = 'Deposits_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid Product for Mobile Deposit', 'Deposits_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Devices')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Device Management', DisplayName = 'Device Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Devices'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Devices', 'Device Management', 'Device Management', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Devices'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access device management', Tag = '', FullyQualifiedName = 'Devices_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access device management', 'Devices_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteTrustedDevice' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete trusted device', Tag = '', FullyQualifiedName = 'Devices_DeleteTrustedDevice', IsAdmin = 0
   WHERE Name = 'DeleteTrustedDevice' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteTrustedDevice', 'Delete trusted device', 'Devices_DeleteTrustedDevice', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EnableBiometricAuthentication' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Enable biometric authentication', Tag = '', FullyQualifiedName = 'Devices_EnableBiometricAuthentication', IsAdmin = 0
   WHERE Name = 'EnableBiometricAuthentication' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EnableBiometricAuthentication', 'Enable biometric authentication', 'Devices_EnableBiometricAuthentication', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DisableBiometricAuthentication' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Disable biometric authentication', Tag = '', FullyQualifiedName = 'Devices_DisableBiometricAuthentication', IsAdmin = 0
   WHERE Name = 'DisableBiometricAuthentication' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DisableBiometricAuthentication', 'Disable biometric authentication', 'Devices_DisableBiometricAuthentication', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Documents')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Document Management', DisplayName = 'Document Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Documents'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Documents', 'Document Management', 'Document Management', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Documents'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access document management', Tag = '', FullyQualifiedName = 'Documents_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access document management', 'Documents_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateStatementSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update statement settings', Tag = '', FullyQualifiedName = 'Documents_UpdateStatementSettings', IsAdmin = 0
   WHERE Name = 'UpdateStatementSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateStatementSettings', 'Update statement settings', 'Documents_UpdateStatementSettings', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'LoanPayoff')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'LoanPayoff', DisplayName = 'LoanPayoff', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'LoanPayoff'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'LoanPayoff', 'LoanPayoff', 'LoanPayoff', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'LoanPayoff'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access loan payoff calculator', Tag = '', FullyQualifiedName = 'LoanPayoff_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access loan payoff calculator', 'LoanPayoff_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'LoanPayoff_Calculate'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Calculate' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Payoff Calculation', Tag = '', FullyQualifiedName = 'LoanPayoff_Calculate', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Calculate' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Calculate', 'Valid Product for Payoff Calculation', 'LoanPayoff_Calculate', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'ProfileInformation')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Profile Information', DisplayName = 'Profile Information', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'ProfileInformation'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'ProfileInformation', 'Profile Information', 'Profile Information', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'ProfileInformation'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Profile Information ', Tag = '', FullyQualifiedName = 'ProfileInformation_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Profile Information ', 'ProfileInformation_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add an address', Tag = '', FullyQualifiedName = 'ProfileInformation_AddAddress', IsAdmin = 0
   WHERE Name = 'AddAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddAddress', 'Add an address', 'ProfileInformation_AddAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit an address', Tag = '', FullyQualifiedName = 'ProfileInformation_EditAddress', IsAdmin = 0
   WHERE Name = 'EditAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditAddress', 'Edit an address', 'ProfileInformation_EditAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an address', Tag = '', FullyQualifiedName = 'ProfileInformation_DeleteAddress', IsAdmin = 0
   WHERE Name = 'DeleteAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteAddress', 'Delete an address', 'ProfileInformation_DeleteAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddPhoneNumber' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add a phone number', Tag = '', FullyQualifiedName = 'ProfileInformation_AddPhoneNumber', IsAdmin = 0
   WHERE Name = 'AddPhoneNumber' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddPhoneNumber', 'Add a phone number', 'ProfileInformation_AddPhoneNumber', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditPhoneNumber' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit a phone number', Tag = '', FullyQualifiedName = 'ProfileInformation_EditPhoneNumber', IsAdmin = 0
   WHERE Name = 'EditPhoneNumber' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditPhoneNumber', 'Edit a phone number', 'ProfileInformation_EditPhoneNumber', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeletePhoneNumber' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete a phone number', Tag = '', FullyQualifiedName = 'ProfileInformation_DeletePhoneNumber', IsAdmin = 0
   WHERE Name = 'DeletePhoneNumber' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeletePhoneNumber', 'Delete a phone number', 'ProfileInformation_DeletePhoneNumber', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddEmailAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add an email address', Tag = '', FullyQualifiedName = 'ProfileInformation_AddEmailAddress', IsAdmin = 0
   WHERE Name = 'AddEmailAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddEmailAddress', 'Add an email address', 'ProfileInformation_AddEmailAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditEmailAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit an email address', Tag = '', FullyQualifiedName = 'ProfileInformation_EditEmailAddress', IsAdmin = 0
   WHERE Name = 'EditEmailAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditEmailAddress', 'Edit an email address', 'ProfileInformation_EditEmailAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteEmailAddress' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an email address', Tag = '', FullyQualifiedName = 'ProfileInformation_DeleteEmailAddress', IsAdmin = 0
   WHERE Name = 'DeleteEmailAddress' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteEmailAddress', 'Delete an email address', 'ProfileInformation_DeleteEmailAddress', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateNickname' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update nickname', Tag = '', FullyQualifiedName = 'ProfileInformation_UpdateNickname', IsAdmin = 0
   WHERE Name = 'UpdateNickname' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateNickname', 'Update nickname', 'ProfileInformation_UpdateNickname', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ChangePassword' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update password', Tag = '', FullyQualifiedName = 'ProfileInformation_ChangePassword', IsAdmin = 0
   WHERE Name = 'ChangePassword' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ChangePassword', 'Update password', 'ProfileInformation_ChangePassword', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ChangeUsername' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update username', Tag = '', FullyQualifiedName = 'ProfileInformation_ChangeUsername', IsAdmin = 0
   WHERE Name = 'ChangeUsername' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ChangeUsername', 'Update username', 'ProfileInformation_ChangeUsername', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'UserActivity')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'User Activity', DisplayName = 'User Activity', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'UserActivity'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'UserActivity', 'User Activity', 'User Activity', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'UserActivity'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access User Activity', Tag = '', FullyQualifiedName = 'UserActivity_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access User Activity', 'UserActivity_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'UserSettings')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'User Settings', DisplayName = 'User Settings', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'UserSettings'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'UserSettings', 'User Settings', 'User Settings', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'UserSettings'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access user settings', Tag = '', FullyQualifiedName = 'UserSettings_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access user settings', 'UserSettings_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'UpdateTimeout' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update timeout setting', Tag = '', FullyQualifiedName = 'UserSettings_UpdateTimeout', IsAdmin = 0
   WHERE Name = 'UpdateTimeout' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'UpdateTimeout', 'Update timeout setting', 'UserSettings_UpdateTimeout', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'SecureMessages')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Secure Messages', DisplayName = 'Secure Messages', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'SecureMessages'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'SecureMessages', 'Secure Messages', 'Secure Messages', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'SecureMessages'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access secure messages', Tag = '', FullyQualifiedName = 'SecureMessages_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access secure messages', 'SecureMessages_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'SendMessage' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate new message/reply', Tag = '', FullyQualifiedName = 'SecureMessages_SendMessage', IsAdmin = 0
   WHERE Name = 'SendMessage' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'SendMessage', 'Initiate new message/reply', 'SecureMessages_SendMessage', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteMessage' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an existing message', Tag = '', FullyQualifiedName = 'SecureMessages_DeleteMessage', IsAdmin = 0
   WHERE Name = 'DeleteMessage' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteMessage', 'Delete an existing message', 'SecureMessages_DeleteMessage', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'StopPayments')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Stop Payments', DisplayName = 'Stop Payments', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'StopPayments'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'StopPayments', 'Stop Payments', 'Stop Payments', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'StopPayments'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access stop payments', Tag = '', FullyQualifiedName = 'StopPayments_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access stop payments', 'StopPayments_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add stop payment', Tag = '', FullyQualifiedName = 'StopPayments_Create', IsAdmin = 0
   WHERE Name = 'Create' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create', 'Add stop payment', 'StopPayments_Create', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit stop payments', Tag = '', FullyQualifiedName = 'StopPayments_Edit', IsAdmin = 0
   WHERE Name = 'Edit' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit', 'Edit stop payments', 'StopPayments_Edit', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Deactivate' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Deactivate existing stop payments', Tag = '', FullyQualifiedName = 'StopPayments_Deactivate', IsAdmin = 0
   WHERE Name = 'Deactivate' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Deactivate', 'Deactivate existing stop payments', 'StopPayments_Deactivate', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'StopPayments_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid product for check stop payment', Tag = '', FullyQualifiedName = 'StopPayments_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid product for check stop payment', 'StopPayments_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'StopPayments_EligibleAch'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'EligibleAch' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid product for ACH stop payment', Tag = '', FullyQualifiedName = 'StopPayments_EligibleAch', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'EligibleAch' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EligibleAch', 'Valid product for ACH stop payment', 'StopPayments_EligibleAch', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Transfer')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Transfers', DisplayName = 'Transfers', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Transfer'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Transfer', 'Transfers', 'Transfers', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Transfer'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access transfers', Tag = '', FullyQualifiedName = 'Transfer_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access transfers', 'Transfer_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate transfers', Tag = '', FullyQualifiedName = 'Transfer_Create', IsAdmin = 0
   WHERE Name = 'Create' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create', 'Initiate transfers', 'Transfer_Create', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit scheduled transfers', Tag = '', FullyQualifiedName = 'Transfer_Edit', IsAdmin = 0
   WHERE Name = 'Edit' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit', 'Edit scheduled transfers', 'Transfer_Edit', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete scheduled transfers', Tag = '', FullyQualifiedName = 'Transfer_Delete', IsAdmin = 0
   WHERE Name = 'Delete' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete', 'Delete scheduled transfers', 'Transfer_Delete', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_ImmediateFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ImmediateFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Immediate Transfer Source', Tag = '', FullyQualifiedName = 'Transfer_ImmediateFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ImmediateFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ImmediateFrom', 'Valid Product for Immediate Transfer Source', 'Transfer_ImmediateFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_ImmediateTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ImmediateTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Immediate Transfer Destination', Tag = '', FullyQualifiedName = 'Transfer_ImmediateTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ImmediateTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ImmediateTo', 'Valid Product for Immediate Transfer Destination', 'Transfer_ImmediateTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_ScheduledFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ScheduledFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Scheduled One-Time Transfer Source', Tag = '', FullyQualifiedName = 'Transfer_ScheduledFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ScheduledFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ScheduledFrom', 'Valid Product for Scheduled One-Time Transfer Source', 'Transfer_ScheduledFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_ScheduledTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ScheduledTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Scheduled One-Time Transfer Destination', Tag = '', FullyQualifiedName = 'Transfer_ScheduledTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ScheduledTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ScheduledTo', 'Valid Product for Scheduled One-Time Transfer Destination', 'Transfer_ScheduledTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_RecurringFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'RecurringFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Recurring Transfer Source', Tag = '', FullyQualifiedName = 'Transfer_RecurringFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'RecurringFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'RecurringFrom', 'Valid Product for Recurring Transfer Source', 'Transfer_RecurringFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Transfer_RecurringTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'RecurringTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Recurring Transfer Destination', Tag = '', FullyQualifiedName = 'Transfer_RecurringTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'RecurringTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'RecurringTo', 'Valid Product for Recurring Transfer Destination', 'Transfer_RecurringTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'CreditAdvance')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'CreditAdvance', DisplayName = 'CreditAdvance', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'CreditAdvance'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'CreditAdvance', 'CreditAdvance', 'CreditAdvance', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'CreditAdvance'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can access the credit line advance feature', Tag = '', FullyQualifiedName = 'CreditAdvance_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Can access the credit line advance feature', 'CreditAdvance_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can post credit line advances from eligible accounts', Tag = '', FullyQualifiedName = 'CreditAdvance_Create', IsAdmin = 0
   WHERE Name = 'Create' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create', 'Can post credit line advances from eligible accounts', 'CreditAdvance_Create', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'CreditAdvance_AdvanceSource'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'AdvanceSource' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Credit Line Advance Source', Tag = '', FullyQualifiedName = 'CreditAdvance_AdvanceSource', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'AdvanceSource' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AdvanceSource', 'Valid Product for Credit Line Advance Source', 'CreditAdvance_AdvanceSource', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'CreditAdvance_AdvanceTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'AdvanceTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Credit Line Advance funds deposit', Tag = '', FullyQualifiedName = 'CreditAdvance_AdvanceTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'AdvanceTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AdvanceTo', 'Valid Product for Credit Line Advance funds deposit', 'CreditAdvance_AdvanceTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'MemberToMember')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Transfers from one account to accounts within the same institution held by others', DisplayName = 'Member-to-Member Transfers', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'MemberToMember'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'MemberToMember', 'Transfers from one account to accounts within the same institution held by others', 'Member-to-Member Transfers', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'MemberToMember'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Member-to-Member transfers', Tag = '', FullyQualifiedName = 'MemberToMember_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Member-to-Member transfers', 'MemberToMember_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Transfer' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate transfer to Member Recipient', Tag = '', FullyQualifiedName = 'MemberToMember_Transfer', IsAdmin = 0
   WHERE Name = 'Transfer' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Transfer', 'Initiate transfer to Member Recipient', 'MemberToMember_Transfer', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Add Recipient' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add Member Recipient', Tag = '', FullyQualifiedName = 'MemberToMember_AddRecipient', IsAdmin = 0
   WHERE Name = 'Add Recipient' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Add Recipient', 'Add Member Recipient', 'MemberToMember_AddRecipient', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit Recipient' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Member Recipients', Tag = '', FullyQualifiedName = 'MemberToMember_EditRecipient', IsAdmin = 0
   WHERE Name = 'Edit Recipient' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit Recipient', 'Edit Member Recipients', 'MemberToMember_EditRecipient', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete Recipient' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete Member Recipients', Tag = '', FullyQualifiedName = 'MemberToMember_DeleteRecipient', IsAdmin = 0
   WHERE Name = 'Delete Recipient' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete Recipient', 'Delete Member Recipients', 'MemberToMember_DeleteRecipient', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'MemberToMember_TransferFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'TransferFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Member-to-Member Transfer Source', Tag = '', FullyQualifiedName = 'MemberToMember_TransferFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'TransferFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'TransferFrom', 'Valid Product for Member-to-Member Transfer Source', 'MemberToMember_TransferFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'TransactionHistory')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'TransactionHistory', DisplayName = 'TransactionHistory', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'TransactionHistory'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'TransactionHistory', 'TransactionHistory', 'TransactionHistory', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'TransactionHistory'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access transaction history', Tag = '', FullyQualifiedName = 'TransactionHistory_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access transaction history', 'TransactionHistory_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'TransactionHistory_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Transaction History Display', Tag = '', FullyQualifiedName = 'TransactionHistory_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid Product for Transaction History Display', 'TransactionHistory_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'TransactionHistoryExport')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'History Export', DisplayName = 'History Export', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'TransactionHistoryExport'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'TransactionHistoryExport', 'History Export', 'History Export', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'TransactionHistoryExport'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access history export', Tag = '', FullyQualifiedName = 'TransactionHistoryExport_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access history export', 'TransactionHistoryExport_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'TransactionHistoryExport_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Transaction History Export', Tag = '', FullyQualifiedName = 'TransactionHistoryExport_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid Product for Transaction History Export', 'TransactionHistoryExport_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'GlobalAnnouncements')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Global Announcements', DisplayName = 'Global Announcements', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'GlobalAnnouncements'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'GlobalAnnouncements', 'Global Announcements', 'Global Announcements', '', '', 0, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'GlobalAnnouncements'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Manage Announcements', Tag = '', FullyQualifiedName = 'GlobalAnnouncements_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Manage Announcements', 'GlobalAnnouncements_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'View' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View announcement details ', Tag = '', FullyQualifiedName = 'GlobalAnnouncements_View', IsAdmin = 1
   WHERE Name = 'View' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'View', 'View announcement details ', 'GlobalAnnouncements_View', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddEdit' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add or edit an announcement but not publish', Tag = '', FullyQualifiedName = 'GlobalAnnouncements_AddEdit', IsAdmin = 1
   WHERE Name = 'AddEdit' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddEdit', 'Add or edit an announcement but not publish', 'GlobalAnnouncements_AddEdit', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'PublishDeactivate' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Publish and/or deactivate announcements', Tag = '', FullyQualifiedName = 'GlobalAnnouncements_PublishDeactivate', IsAdmin = 1
   WHERE Name = 'PublishDeactivate' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'PublishDeactivate', 'Publish and/or deactivate announcements', 'GlobalAnnouncements_PublishDeactivate', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an announcement', Tag = '', FullyQualifiedName = 'GlobalAnnouncements_Delete', IsAdmin = 1
   WHERE Name = 'Delete' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete', 'Delete an announcement', 'GlobalAnnouncements_Delete', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'EndUserManagement')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'End User Management', DisplayName = 'End User Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'EndUserManagement'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'EndUserManagement', 'End User Management', 'End User Management', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'EndUserManagement'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access End User Management', Tag = '', FullyQualifiedName = 'EndUserManagement_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access End User Management', 'EndUserManagement_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewEndUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View end user information', Tag = '', FullyQualifiedName = 'EndUserManagement_ViewEndUser', IsAdmin = 1
   WHERE Name = 'ViewEndUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewEndUser', 'View end user information', 'EndUserManagement_ViewEndUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ManageEndUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit end user information', Tag = '', FullyQualifiedName = 'EndUserManagement_ManageEndUser', IsAdmin = 1
   WHERE Name = 'ManageEndUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ManageEndUser', 'Edit end user information', 'EndUserManagement_ManageEndUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'SupportSignOn' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Enable/Disable Support Sign On for end user', Tag = '', FullyQualifiedName = 'EndUserManagement_SupportSignOn', IsAdmin = 1
   WHERE Name = 'SupportSignOn' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'SupportSignOn', 'Enable/Disable Support Sign On for end user', 'EndUserManagement_SupportSignOn', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewGlobalEndUserActivityLog' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Global End User Activity Log', Tag = '', FullyQualifiedName = 'EndUserManagement_ViewGlobalEndUserActivityLog', IsAdmin = 1
   WHERE Name = 'ViewGlobalEndUserActivityLog' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewGlobalEndUserActivityLog', 'View Global End User Activity Log', 'EndUserManagement_ViewGlobalEndUserActivityLog', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DisableUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Allows permanently disabling a user’s ability to login to the application', Tag = '', FullyQualifiedName = 'EndUserManagement_DisableUser', IsAdmin = 1
   WHERE Name = 'DisableUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DisableUser', 'Allows permanently disabling a user’s ability to login to the application', 'EndUserManagement_DisableUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'TemporaryPassword' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Allows assigning a temporary password to an end-user', Tag = '', FullyQualifiedName = 'EndUserManagement_TemporaryPassword', IsAdmin = 1
   WHERE Name = 'TemporaryPassword' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'TemporaryPassword', 'Allows assigning a temporary password to an end-user', 'EndUserManagement_TemporaryPassword', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'LockUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Allows temporarily locking an end user’s account', Tag = '', FullyQualifiedName = 'EndUserManagement_LockUser', IsAdmin = 1
   WHERE Name = 'LockUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'LockUser', 'Allows temporarily locking an end user’s account', 'EndUserManagement_LockUser', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'EmployeeManagement')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Employee Management', DisplayName = 'Employee Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'EmployeeManagement'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'EmployeeManagement', 'Employee Management', 'Employee Management', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'EmployeeManagement'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Employee Management', Tag = '', FullyQualifiedName = 'EmployeeManagement_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Employee Management', 'EmployeeManagement_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'CreateAdminUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add Admin (Employee) user', Tag = '', FullyQualifiedName = 'EmployeeManagement_CreateAdminUser', IsAdmin = 1
   WHERE Name = 'CreateAdminUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CreateAdminUser', 'Add Admin (Employee) user', 'EmployeeManagement_CreateAdminUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewAdminUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Admin (Employee) user', Tag = '', FullyQualifiedName = 'EmployeeManagement_ViewAdminUser', IsAdmin = 1
   WHERE Name = 'ViewAdminUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewAdminUser', 'View Admin (Employee) user', 'EmployeeManagement_ViewAdminUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ManageAdminUser' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Admin (Employee) user information', Tag = '', FullyQualifiedName = 'EmployeeManagement_ManageAdminUser', IsAdmin = 1
   WHERE Name = 'ManageAdminUser' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ManageAdminUser', 'Edit Admin (Employee) user information', 'EmployeeManagement_ManageAdminUser', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AssignAdminRoles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Assign/Unassign roles to employee user', Tag = '', FullyQualifiedName = 'EmployeeManagement_AssignAdminRoles', IsAdmin = 1
   WHERE Name = 'AssignAdminRoles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AssignAdminRoles', 'Assign/Unassign roles to employee user', 'EmployeeManagement_AssignAdminRoles', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewAdminUserActivityLog' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Admin User Activity Log', Tag = '', FullyQualifiedName = 'EmployeeManagement_ViewAdminUserActivityLog', IsAdmin = 1
   WHERE Name = 'ViewAdminUserActivityLog' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewAdminUserActivityLog', 'View Admin User Activity Log', 'EmployeeManagement_ViewAdminUserActivityLog', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Configuration')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Configuration', DisplayName = 'Configuration', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'Configuration'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Configuration', 'Configuration', 'Configuration', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Configuration'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Configuration', Tag = '', FullyQualifiedName = 'Configuration_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Configuration', 'Configuration_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditGlobalSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update Global Configuration', Tag = '', FullyQualifiedName = 'Configuration_EditGlobalSettings', IsAdmin = 1
   WHERE Name = 'EditGlobalSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditGlobalSettings', 'Update Global Configuration', 'Configuration_EditGlobalSettings', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewGlobalSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Global Configurations', Tag = '', FullyQualifiedName = 'Configuration_ViewGlobalSettings', IsAdmin = 1
   WHERE Name = 'ViewGlobalSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewGlobalSettings', 'View Global Configurations', 'Configuration_ViewGlobalSettings', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditAdminSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Update Admin-specific settings', Tag = '', FullyQualifiedName = 'Configuration_EditAdminSettings', IsAdmin = 1
   WHERE Name = 'EditAdminSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditAdminSettings', 'Update Admin-specific settings', 'Configuration_EditAdminSettings', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewAdminSettings' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Admin-specific settings', Tag = '', FullyQualifiedName = 'Configuration_ViewAdminSettings', IsAdmin = 1
   WHERE Name = 'ViewAdminSettings' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewAdminSettings', 'View Admin-specific settings', 'Configuration_ViewAdminSettings', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'AdminUserPermissions')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Admin User Permissions', DisplayName = 'Admin User Permissions', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'AdminUserPermissions'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'AdminUserPermissions', 'Admin User Permissions', 'Admin User Permissions', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'AdminUserPermissions'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Admin User Permissions', Tag = '', FullyQualifiedName = 'AdminUserPermissions_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Admin User Permissions', 'AdminUserPermissions_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditAdminRoles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Admin (Employee) role permissions', Tag = '', FullyQualifiedName = 'AdminUserPermissions_EditAdminRoles', IsAdmin = 1
   WHERE Name = 'EditAdminRoles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditAdminRoles', 'Edit Admin (Employee) role permissions', 'AdminUserPermissions_EditAdminRoles', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewAdminRoles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Admin (Employee) role permissions', Tag = '', FullyQualifiedName = 'AdminUserPermissions_ViewAdminRoles', IsAdmin = 1
   WHERE Name = 'ViewAdminRoles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewAdminRoles', 'View Admin (Employee) role permissions', 'AdminUserPermissions_ViewAdminRoles', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'EndUserPermissions')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'End User Permissions', DisplayName = 'End User Permissions', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'EndUserPermissions'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'EndUserPermissions', 'End User Permissions', 'End User Permissions', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'EndUserPermissions'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access End User Permissions', Tag = '', FullyQualifiedName = 'EndUserPermissions_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access End User Permissions', 'EndUserPermissions_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditFeatures' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Features in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_EditFeatures', IsAdmin = 1
   WHERE Name = 'EditFeatures' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditFeatures', 'Edit Features in Permissions Framework', 'EndUserPermissions_EditFeatures', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewFeatures' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Features in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_ViewFeatures', IsAdmin = 1
   WHERE Name = 'ViewFeatures' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewFeatures', 'View Features in Permissions Framework', 'EndUserPermissions_ViewFeatures', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditProductTable' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Product Table in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_EditProductTable', IsAdmin = 1
   WHERE Name = 'EditProductTable' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditProductTable', 'Edit Product Table in Permissions Framework', 'EndUserPermissions_EditProductTable', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewProductTable' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Product Table in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_ViewProductTable', IsAdmin = 1
   WHERE Name = 'ViewProductTable' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewProductTable', 'View Product Table in Permissions Framework', 'EndUserPermissions_ViewProductTable', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditUserRoles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit End User Roles in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_EditUserRoles', IsAdmin = 1
   WHERE Name = 'EditUserRoles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditUserRoles', 'Edit End User Roles in Permissions Framework', 'EndUserPermissions_EditUserRoles', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewUserRoles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View End User Roles in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_ViewUserRoles', IsAdmin = 1
   WHERE Name = 'ViewUserRoles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewUserRoles', 'View End User Roles in Permissions Framework', 'EndUserPermissions_ViewUserRoles', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditMetadataGroups' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Metadata Groups in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_EditMetadataGroups', IsAdmin = 1
   WHERE Name = 'EditMetadataGroups' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditMetadataGroups', 'Edit Metadata Groups in Permissions Framework', 'EndUserPermissions_EditMetadataGroups', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewMetadataGroups' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Metadata Groups in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_ViewMetadataGroups', IsAdmin = 1
   WHERE Name = 'ViewMetadataGroups' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewMetadataGroups', 'View Metadata Groups in Permissions Framework', 'EndUserPermissions_ViewMetadataGroups', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditMetadataDefinitions' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit Metadata Definitions in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_EditMetadataDefinitions', IsAdmin = 1
   WHERE Name = 'EditMetadataDefinitions' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditMetadataDefinitions', 'Edit Metadata Definitions in Permissions Framework', 'EndUserPermissions_EditMetadataDefinitions', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewMetadataDefinitions' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View Metadata Definitions in Permissions Framework', Tag = '', FullyQualifiedName = 'EndUserPermissions_ViewMetadataDefinitions', IsAdmin = 1
   WHERE Name = 'ViewMetadataDefinitions' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewMetadataDefinitions', 'View Metadata Definitions in Permissions Framework', 'EndUserPermissions_ViewMetadataDefinitions', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'SupportSignOn')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Support Sign On', DisplayName = 'Support Sign On', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'SupportSignOn'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'SupportSignOn', 'Support Sign On', 'Support Sign On', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'SupportSignOn'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate Support Sign On to an end user', Tag = '', FullyQualifiedName = 'SupportSignOn_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Initiate Support Sign On to an end user', 'SupportSignOn_Access', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Payments')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Permissions for accessing and managing loan payments', DisplayName = 'Loan Payments', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Payments'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Payments', 'Permissions for accessing and managing loan payments', 'Loan Payments', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Payments'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Pay Loan or Credit Card and view current status of scheduled payments', Tag = '', FullyQualifiedName = 'Payments_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Pay Loan or Credit Card and view current status of scheduled payments', 'Payments_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate loan/credit card payments', Tag = '', FullyQualifiedName = 'Payments_Create', IsAdmin = 0
   WHERE Name = 'Create' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create', 'Initiate loan/credit card payments', 'Payments_Create', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit scheduled loan/credit card payments', Tag = '', FullyQualifiedName = 'Payments_Edit', IsAdmin = 0
   WHERE Name = 'Edit' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit', 'Edit scheduled loan/credit card payments', 'Payments_Edit', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete scheduled loan/credit card payments', Tag = '', FullyQualifiedName = 'Payments_Delete', IsAdmin = 0
   WHERE Name = 'Delete' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete', 'Delete scheduled loan/credit card payments', 'Payments_Delete', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_PrincipalOnlyPayment'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'PrincipalOnlyLoanPayment' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Principal Only Payments', Tag = '', FullyQualifiedName = 'Payments_PrincipalOnlyPayment', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'PrincipalOnlyLoanPayment' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'PrincipalOnlyLoanPayment', 'Valid Product for Principal Only Payments', 'Payments_PrincipalOnlyPayment', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_AllowVariablePayments'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'AllowVariablePayments' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Allow selection of variable monthly payment amounts such as ''minimum due'',''statement balance'', etc.', Tag = '', FullyQualifiedName = 'Payments_AllowVariablePayments', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'AllowVariablePayments' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AllowVariablePayments', 'Allow selection of variable monthly payment amounts such as ''minimum due'',''statement balance'', etc.', 'Payments_AllowVariablePayments', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_ImmediateFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ImmediateFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Immediate Payment Source', Tag = '', FullyQualifiedName = 'Payments_ImmediateFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ImmediateFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ImmediateFrom', 'Valid Product for Immediate Payment Source', 'Payments_ImmediateFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_ImmediateTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ImmediateTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Immediate Loan Payments', Tag = '', FullyQualifiedName = 'Payments_ImmediateTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ImmediateTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ImmediateTo', 'Valid Product for Immediate Loan Payments', 'Payments_ImmediateTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_ScheduledFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ScheduledFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Scheduled One-Time Payment Source', Tag = '', FullyQualifiedName = 'Payments_ScheduledFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ScheduledFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ScheduledFrom', 'Valid Product for Scheduled One-Time Payment Source', 'Payments_ScheduledFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_ScheduledTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'ScheduledTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Scheduled One-Time Loan Payments', Tag = '', FullyQualifiedName = 'Payments_ScheduledTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'ScheduledTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ScheduledTo', 'Valid Product for Scheduled One-Time Loan Payments', 'Payments_ScheduledTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_RecurringFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'RecurringFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Recurring Payment Source', Tag = '', FullyQualifiedName = 'Payments_RecurringFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'RecurringFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'RecurringFrom', 'Valid Product for Recurring Payment Source', 'Payments_RecurringFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'Payments_RecurringTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'RecurringTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for Recurring Loan Payments', Tag = '', FullyQualifiedName = 'Payments_RecurringTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'RecurringTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'RecurringTo', 'Valid Product for Recurring Loan Payments', 'Payments_RecurringTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'ExternalTransfers')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'External Transfers', DisplayName = 'External Transfers', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'ExternalTransfers'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'ExternalTransfers', 'External Transfers', 'External Transfers', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'ExternalTransfers'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Manage External Accounts', Tag = '', FullyQualifiedName = 'ExternalTransfers_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Manage External Accounts', 'ExternalTransfers_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ViewAccounts' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View existing external account details', Tag = '', FullyQualifiedName = 'ExternalTransfers_ViewAccounts', IsAdmin = 0
   WHERE Name = 'ViewAccounts' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ViewAccounts', 'View existing external account details', 'ExternalTransfers_ViewAccounts', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AddAccounts' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Add external accounts', Tag = '', FullyQualifiedName = 'ExternalTransfers_AddAccount', IsAdmin = 0
   WHERE Name = 'AddAccounts' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AddAccounts', 'Add external accounts', 'ExternalTransfers_AddAccount', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'EditAccounts' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit an external account', Tag = '', FullyQualifiedName = 'ExternalTransfers_EditAccount', IsAdmin = 0
   WHERE Name = 'EditAccounts' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'EditAccounts', 'Edit an external account', 'ExternalTransfers_EditAccount', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'DeleteAccounts' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an external account', Tag = '', FullyQualifiedName = 'ExternalTransfers_DeleteAccount', IsAdmin = 0
   WHERE Name = 'DeleteAccounts' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DeleteAccounts', 'Delete an external account', 'ExternalTransfers_DeleteAccount', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'ExternalTransfers_TransferFrom'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'TransferFrom' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for External Transfer Source', Tag = '', FullyQualifiedName = 'ExternalTransfers_TransferFrom', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'TransferFrom' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'TransferFrom', 'Valid Product for External Transfer Source', 'ExternalTransfers_TransferFrom', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'ExternalTransfers_TransferTo'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'TransferTo' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for External Transfer Destination', Tag = '', FullyQualifiedName = 'ExternalTransfers_TransferTo', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'TransferTo' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'TransferTo', 'Valid Product for External Transfer Destination', 'ExternalTransfers_TransferTo', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'UserAnnouncements')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'User Announcements', DisplayName = 'User Announcements', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'UserAnnouncements'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'UserAnnouncements', 'User Announcements', 'User Announcements', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'UserAnnouncements'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'View' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'View announcements', Tag = '', FullyQualifiedName = 'UserAnnouncements_View', IsAdmin = 0
   WHERE Name = 'View' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'View', 'View announcements', 'UserAnnouncements_View', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete an announcement', Tag = '', FullyQualifiedName = 'UserAnnouncements_Delete', IsAdmin = 0
   WHERE Name = 'Delete' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete', 'Delete an announcement', 'UserAnnouncements_Delete', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'AccountStatements')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Account Statements', DisplayName = 'Account Statements', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'AccountStatements'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'AccountStatements', 'Account Statements', 'Account Statements', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'AccountStatements'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can view account statement settings', Tag = '', FullyQualifiedName = 'AccountStatements_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Can view account statement settings', 'AccountStatements_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can edit account statement settings', Tag = '', FullyQualifiedName = 'AccountStatements_Edit', IsAdmin = 0
   WHERE Name = 'Edit' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit', 'Can edit account statement settings', 'AccountStatements_Edit', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountStatements_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid product for modifying account statement settings', Tag = '', FullyQualifiedName = 'AccountStatements_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid product for modifying account statement settings', 'AccountStatements_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountStatements_DepositGroup'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'DepositGroup' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Product will have account statement settings applied based on the selection for the Deposit group', Tag = '', FullyQualifiedName = 'AccountStatements_DepositGroup', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'DepositGroup' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'DepositGroup', 'Product will have account statement settings applied based on the selection for the Deposit group', 'AccountStatements_DepositGroup', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountStatements_LoanGroup'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'LoanGroup' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Product will have account statement settings applied based on the selection for the Loan group', Tag = '', FullyQualifiedName = 'AccountStatements_LoanGroup', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'LoanGroup' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'LoanGroup', 'Product will have account statement settings applied based on the selection for the Loan group', 'AccountStatements_LoanGroup', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountStatements_MortgageGroup'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'MortgageGroup' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Product will have account statement settings applied based on the selection for the Mortgage group', Tag = '', FullyQualifiedName = 'AccountStatements_MortgageGroup', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'MortgageGroup' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'MortgageGroup', 'Product will have account statement settings applied based on the selection for the Mortgage group', 'AccountStatements_MortgageGroup', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'AccountStatements_CreditCardGroup'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'CreditCardGroup' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Product will have account statement settings applied based on the selection for the Credit Card group', Tag = '', FullyQualifiedName = 'AccountStatements_CreditCardGroup', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'CreditCardGroup' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CreditCardGroup', 'Product will have account statement settings applied based on the selection for the Credit Card group', 'AccountStatements_CreditCardGroup', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'MoveMoney')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Move Money', DisplayName = 'Move Money', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'MoveMoney'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'MoveMoney', 'Move Money', 'Move Money', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'MoveMoney'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Profile')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Profile', DisplayName = 'Profile', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Profile'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Profile', 'Profile', 'Profile', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Profile'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'ContactUs')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Contact Us', DisplayName = 'Contact Us', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'ContactUs'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'ContactUs', 'Contact Us', 'Contact Us', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'ContactUs'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'PersonalFinanceManagement')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Personal Finance Management', DisplayName = 'Personal Finance Management', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'PersonalFinanceManagement'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'PersonalFinanceManagement', 'Personal Finance Management', 'Personal Finance Management', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'PersonalFinanceManagement'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access personal finance management', Tag = '', FullyQualifiedName = 'PersonalFinanceManagement_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access personal finance management', 'PersonalFinanceManagement_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Marketing')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Display marketing spots', DisplayName = 'Marketing', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'Marketing'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Marketing', 'Display marketing spots', 'Marketing', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Marketing'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Marketing', Tag = '', FullyQualifiedName = 'Marketing_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Marketing', 'Marketing_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Dashboard' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Marketing Dashboard', Tag = '', FullyQualifiedName = 'Marketing_Dashboard', IsAdmin = 1
   WHERE Name = 'Dashboard' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Dashboard', 'Access Marketing Dashboard', 'Marketing_Dashboard', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Business')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Display Business', DisplayName = 'Business', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Business'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Business', 'Display Business', 'Business', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Business'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Business', Tag = '', FullyQualifiedName = 'Business_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Business', 'Business_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AccessACHBatches' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access ACH Batches', Tag = '', FullyQualifiedName = 'BusinessACHBatches_Access', IsAdmin = 0
   WHERE Name = 'AccessACHBatches' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AccessACHBatches', 'Access ACH Batches', 'BusinessACHBatches_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ReadyACHBatches' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Ready ACH Batches', Tag = '', FullyQualifiedName = 'BusinessACHBatches_Ready', IsAdmin = 0
   WHERE Name = 'ReadyACHBatches' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ReadyACHBatches', 'Ready ACH Batches', 'BusinessACHBatches_Ready', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'QueueACHBatches' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Queue ACH Batches', Tag = '', FullyQualifiedName = 'BusinessACHBatches_Queue', IsAdmin = 0
   WHERE Name = 'QueueACHBatches' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'QueueACHBatches', 'Queue ACH Batches', 'BusinessACHBatches_Queue', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'CompletedACHBatches' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Completed ACH Batches', Tag = '', FullyQualifiedName = 'BusinessACHBatches_Completed', IsAdmin = 0
   WHERE Name = 'CompletedACHBatches' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CompletedACHBatches', 'Completed ACH Batches', 'BusinessACHBatches_Completed', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'AccessWireFiles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Wire Files', Tag = '', FullyQualifiedName = 'BusinessWireFiles_Access', IsAdmin = 0
   WHERE Name = 'AccessWireFiles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'AccessWireFiles', 'Access Wire Files', 'BusinessWireFiles_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'ReadyWireFiles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Ready Wire Files ', Tag = '', FullyQualifiedName = 'BusinessWireFiles_Ready', IsAdmin = 0
   WHERE Name = 'ReadyWireFiles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'ReadyWireFiles', 'Ready Wire Files ', 'BusinessWireFiles_Ready', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'PendingWireFiles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Pending Wire Files', Tag = '', FullyQualifiedName = 'BusinessWireFiles_Pending', IsAdmin = 0
   WHERE Name = 'PendingWireFiles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'PendingWireFiles', 'Pending Wire Files', 'BusinessWireFiles_Pending', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'CompletedWireFiles' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Completed Wire Files', Tag = '', FullyQualifiedName = 'BusinessWireFiles_Completed', IsAdmin = 0
   WHERE Name = 'CompletedWireFiles' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'CompletedWireFiles', 'Completed Wire Files', 'BusinessWireFiles_Completed', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'SystemAdministration')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'System Administration', DisplayName = 'System Administration', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'SystemAdministration'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'SystemAdministration', 'System Administration', 'System Administration', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'SystemAdministration'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access System Administration', Tag = '', FullyQualifiedName = 'SystemAdministration_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access System Administration', 'SystemAdministration_Access', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Status' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'System Administration Status', Tag = '', FullyQualifiedName = 'SystemAdministration_Status', IsAdmin = 1
   WHERE Name = 'Status' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Status', 'System Administration Status', 'SystemAdministration_Status', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Jobs' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'System Administration Jobs', Tag = '', FullyQualifiedName = 'SystemAdministration_Jobs', IsAdmin = 1
   WHERE Name = 'Jobs' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Jobs', 'System Administration Jobs', 'SystemAdministration_Jobs', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Servers' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'System Administration Servers', Tag = '', FullyQualifiedName = 'SystemAdministration_Servers', IsAdmin = 1
   WHERE Name = 'Servers' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Servers', 'System Administration Servers', 'SystemAdministration_Servers', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Processes' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'System Administration Processes', Tag = '', FullyQualifiedName = 'SystemAdministration_Processes', IsAdmin = 1
   WHERE Name = 'Processes' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Processes', 'System Administration Processes', 'SystemAdministration_Processes', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'P2P')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'P2P', DisplayName = 'P2P', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'P2P'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'P2P', 'P2P', 'P2P', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'P2P'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Person-To-Person payments', Tag = '', FullyQualifiedName = 'P2P_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Person-To-Person payments', 'P2P_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'P2P_Source'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Source' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid product as a source for person-to-person payments', Tag = '', FullyQualifiedName = 'P2P_Source', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Source' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Source', 'Valid product as a source for person-to-person payments', 'P2P_Source', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'CheckWithdrawal')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Check Withdrawal Requests', DisplayName = 'Check Withdrawal Requests', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'CheckWithdrawal'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'CheckWithdrawal', 'Check Withdrawal Requests', 'Check Withdrawal Requests', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'CheckWithdrawal'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Check Withdrawal Request', Tag = '', FullyQualifiedName = 'CheckWithdrawal_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Check Withdrawal Request', 'CheckWithdrawal_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Initiate Check Withdrawal Request', Tag = '', FullyQualifiedName = 'CheckWithdrawal_Create', IsAdmin = 0
   WHERE Name = 'Create' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create', 'Initiate Check Withdrawal Request', 'CheckWithdrawal_Create', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'CheckWithdrawal_Source'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Source' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Account for Check Withdrawal Request', Tag = '', FullyQualifiedName = 'CheckWithdrawal_Source', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Source' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Source', 'Valid Account for Check Withdrawal Request', 'CheckWithdrawal_Source', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'BillPaySSO')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Single Sign-On to Bill Pay', DisplayName = 'Bill Pay SSO', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'BillPaySSO'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'BillPaySSO', 'Single Sign-On to Bill Pay', 'Bill Pay SSO', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'BillPaySSO'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Single Sign-On to Bill Pay', Tag = '', FullyQualifiedName = 'BillPaySSO_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Single Sign-On to Bill Pay', 'BillPaySSO_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'P2PSSO')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Single Sign-On to person-to-person payments', DisplayName = 'P2P Payments SSO', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'P2PSSO'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'P2PSSO', 'Single Sign-On to person-to-person payments', 'P2P Payments SSO', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'P2PSSO'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Single Sign-On to P2P Payments', Tag = '', FullyQualifiedName = 'P2PSSO_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Single Sign-On to P2P Payments', 'P2PSSO_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'OpenLoanSSO')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Single Sign-On to open loan accounts', DisplayName = 'Loan Application SSO', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'OpenLoanSSO'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'OpenLoanSSO', 'Single Sign-On to open loan accounts', 'Loan Application SSO', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'OpenLoanSSO'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Single Sign-On to open loan accounts', Tag = '', FullyQualifiedName = 'OpenLoanSSO_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Single Sign-On to open loan accounts', 'OpenLoanSSO_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'OpenDepositSSO')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Single Sign-On to open deposit accounts', DisplayName = 'Deposit Application SSO', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'OpenDepositSSO'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'OpenDepositSSO', 'Single Sign-On to open deposit accounts', 'Deposit Application SSO', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'OpenDepositSSO'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Single Sign-On to open deposit accounts', Tag = '', FullyQualifiedName = 'OpenDepositSSO_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access Single Sign-On to open deposit accounts', 'OpenDepositSSO_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'SkipPay')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Skip Payment', DisplayName = 'Skip Payment', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'SkipPay'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'SkipPay', 'Skip Payment', 'Skip Payment', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'SkipPay'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can access the Skip Payment feature', Tag = '', FullyQualifiedName = 'SkipPay_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Can access the Skip Payment feature', 'SkipPay_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'SkipPay_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid Product for having Skip Payment applied for a loan payment', Tag = '', FullyQualifiedName = 'SkipPay_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid Product for having Skip Payment applied for a loan payment', 'SkipPay_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'SkipPay_FeeSource'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'FeeSource' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Eligible for assessment of Skip Payment fees', Tag = '', FullyQualifiedName = 'SkipPay_FeeSource', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'FeeSource' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'FeeSource', 'Eligible for assessment of Skip Payment fees', 'SkipPay_FeeSource', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'NACHAFile')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'NACHA File download request', DisplayName = 'NACHA File', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 1
   WHERE Name = 'NACHAFile'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'NACHAFile', 'NACHA File download request', 'NACHA File', '', '', 1, 1)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'NACHAFile'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access to download NACHA files', Tag = '', FullyQualifiedName = 'NACHAFile_Access', IsAdmin = 1
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access to download NACHA files', 'NACHAFile_Access', 1)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'VerifyContactInformation')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Enable or disable prompting of contact information verification during login', DisplayName = 'Verify Contact Information - Login', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'VerifyContactInformation'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'VerifyContactInformation', 'Enable or disable prompting of contact information verification during login', 'Verify Contact Information - Login', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'VerifyContactInformation'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Alerts')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Alerts', DisplayName = 'Alerts', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Alerts'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Alerts', 'Alerts', 'Alerts', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Alerts'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access alerts', Tag = '', FullyQualifiedName = 'Alerts_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access alerts', 'Alerts_Access', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'DepositsSSO')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Check Deposit SSO', DisplayName = 'Check Deposit SSO', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'DepositsSSO'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'DepositsSSO', 'Check Deposit SSO', 'Check Deposit SSO', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'DepositsSSO'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access check deposits via SSO in web and/or mobile', Tag = '', FullyQualifiedName = 'DepositsSSO_Access', IsAdmin = 0
   WHERE Name = 'Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Access', 'Access check deposits via SSO in web and/or mobile', 'DepositsSSO_Access', 0)
END
COMMIT TRAN


SET @MetadataDefinitionGroupId = NULL
SELECT @MetadataDefinitionGroupId = id FROM MetadataDefinitionGroups WHERE Name = 'DepositsSSO_Eligible'

BEGIN TRAN
IF EXISTS (SELECT * FROM FeatureProductContexts WITH (updlock,serializable) WHERE Name = 'Eligible' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeatureProductContexts set UpdateAt = GetDate(), Description = 'Valid product for selection of check deposits via SSO', Tag = '', FullyQualifiedName = 'DepositsSSO_Eligible', MetadataDefinitionGroupId = @MetadataDefinitionGroupId
   WHERE Name = 'Eligible' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeatureProductContexts (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, MetadataDefinitionGroupId)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Eligible', 'Valid product for selection of check deposits via SSO', 'DepositsSSO_Eligible', @MetadataDefinitionGroupId)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Categorization')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Categorization Features', DisplayName = 'Categorization Features', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Categorization'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Categorization', 'Categorization Features', 'Categorization Features', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Categorization'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Personal Analytics Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Access Personal Analytics', Tag = '', FullyQualifiedName = 'Categorization_PersonalAnalyticsAccess', IsAdmin = 1
   WHERE Name = 'Personal Analytics Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Personal Analytics Access', 'Access Personal Analytics', 'Categorization_PersonalAnalyticsAccess', 1)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Budgeting Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can view budgeting information.', Tag = '', FullyQualifiedName = 'Categorization_BudgetingAccess', IsAdmin = 0
   WHERE Name = 'Budgeting Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Budgeting Access', 'Can view budgeting information.', 'Categorization_BudgetingAccess', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create Budget' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Create category budget.', Tag = '', FullyQualifiedName = 'Categorization_CreateCategoryBudget', IsAdmin = 0
   WHERE Name = 'Create Budget' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create Budget', 'Create category budget.', 'Categorization_CreateCategoryBudget', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit Budget' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit category budget.', Tag = '', FullyQualifiedName = 'Categorization_EditCategoryBudget', IsAdmin = 0
   WHERE Name = 'Edit Budget' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit Budget', 'Edit category budget.', 'Categorization_EditCategoryBudget', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete Budget' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete category budget.', Tag = '', FullyQualifiedName = 'Categorization_DeleteCategoryBudget', IsAdmin = 0
   WHERE Name = 'Delete Budget' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete Budget', 'Delete category budget.', 'Categorization_DeleteCategoryBudget', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Goals')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Goals Features', DisplayName = 'Goals Features', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Goals'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Goals', 'Goals Features', 'Goals Features', '', '', 0, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Goals'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if

BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Goals Access' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Can view goals information.', Tag = '', FullyQualifiedName = 'Goals_Access', IsAdmin = 0
   WHERE Name = 'Goals Access' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Goals Access', 'Can view goals information.', 'Goals_Access', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Create Goals' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Create goals.', Tag = '', FullyQualifiedName = 'Goals_Create', IsAdmin = 0
   WHERE Name = 'Create Goals' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Create Goals', 'Create goals.', 'Goals_Create', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Edit Goals' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Edit goals.', Tag = '', FullyQualifiedName = 'Goals_Edit', IsAdmin = 0
   WHERE Name = 'Edit Goals' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Edit Goals', 'Edit goals.', 'Goals_Edit', 0)
END
COMMIT TRAN


BEGIN TRAN
IF EXISTS (SELECT * FROM FeaturePermissions WITH (updlock,serializable) WHERE Name = 'Delete Goals' AND FeatureId = @FeatureId)
BEGIN
   UPDATE FeaturePermissions set UpdateAt = GetDate(), Description = 'Delete goals.', Tag = '', FullyQualifiedName = 'Goals_Delete', IsAdmin = 0
   WHERE Name = 'Delete Goals' AND FeatureId = @FeatureId
END
ELSE
BEGIN
   INSERT INTO FeaturePermissions (InsertAt, UpdateAt, FeatureId, Name, Description, FullyQualifiedName, IsAdmin)
   VALUES (GetDate(), GetDate(), @FeatureId, 'Delete Goals', 'Delete goals.', 'Goals_Delete', 0)
END
COMMIT TRAN

end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'MerchantTagging')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Merchant Tagging', DisplayName = 'Merchant Tagging', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'MerchantTagging'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'MerchantTagging', 'Merchant Tagging', 'Merchant Tagging', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'MerchantTagging'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Login')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Login', DisplayName = 'Login', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Login'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Login', 'Login', 'Login', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Login'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end

SET @FeatureId = 0

BEGIN TRAN
IF EXISTS (SELECT * FROM Features WITH (updlock,serializable) WHERE Name = 'Registration')
BEGIN
   UPDATE Features set UpdateAt = GetDate(), Description = 'Registration', DisplayName = 'Registration', BaseApiRoute = '', BaseUiRoute = '', IsAdmin = 0
   WHERE Name = 'Registration'
END
ELSE
BEGIN
   INSERT INTO Features (InsertAt, UpdateAt, Name, Description, DisplayName, BaseApiRoute, BaseUiRoute, Enabled, IsAdmin)
   VALUES (GetDate(), GetDate(), 'Registration', 'Registration', 'Registration', '', '', 1, 0)
END
COMMIT TRAN

SELECT @FeatureId = Id FROM Features WHERE Name = 'Registration'

if(@FeatureId > 0)
begin
set @featureid = @featureid --avoid possible empty if
end
