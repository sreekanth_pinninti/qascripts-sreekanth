
DECLARE @RoleId nvarchar(1000)
DECLARE @adminId nvarchar(max) = (select Top(1) Id From DigitalAdmins);

--Create 'Allow All' Role
IF NOT EXISTS (SELECT * FROM DigitalRoles WHERE Name = 'Dev_Allow_All')
   BEGIN
       INSERT INTO DigitalRoles (Id, InsertAt, UpdateAt, Name, NormalizedName, ConcurrencyStamp, IsAdmin, [Description], IsDefault)
       VALUES (newid(), getdate(), getdate(), 'Dev_Allow_All', 'DEV_ALLOW_ALL', newid(), 0, 'Development Allow All', 0)
   END

SELECT @RoleId = Id FROM DigitalRoles WHERE Name = 'Dev_Allow_All'

--Assign all feaure permissions to the role
INSERT INTO DigitalRolePermissions (InsertAt, UpdateAt, DigitalRoleId, FeaturePermissionId, Allowed)
SELECT getdate(), getdate(), @RoleId, Id, 1 FROM FeaturePermissions
WHERE (Id NOT IN (SELECT FeaturePermissionId FROM DigitalRolePermissions WHERE DigitalRoleId = @RoleId)) AND (IsAdmin <> 1)

--Assign all digital users to the role
INSERT INTO DigitalUserRoles(InsertAt, UpdateAt, DigitalRoleId, DigitalUserId, AssignedBy)
SELECT getdate(), getdate(), @RoleId, Id, @adminId FROM DigitalUsers
WHERE Id NOT IN (SELECT DigitalUserId FROM DigitalUserRoles WHERE DigitalRoleId = @RoleId)
