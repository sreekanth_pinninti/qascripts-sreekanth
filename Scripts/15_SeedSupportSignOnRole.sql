Declare @RoleId nvarchar(450), @PermissionId bigint
Set @RoleId = NEWID()

IF NOT EXISTS (SELECT * FROM DigitalRoles WITH (updlock,serializable) WHERE NormalizedName = 'ADMIN_SUPPORTSIGNON' AND IsAdmin = 1)
BEGIN
	BEGIN TRAN	
	INSERT INTO DigitalRoles
	(Id, CreatedAt, InsertAt, UpdateAt, Name, NormalizedName, Description, ConcurrencyStamp, IsAdmin, CreatedBy, IsDefault)
	VALUES
	(@RoleId, GETUTCDATE(),GETUTCDATE(),GETUTCDATE(), 'Admin_SupportSignOn', 'ADMIN_SUPPORTSIGNON', 'Default Support Sign On Admin Role', '37197452-F1DE-4F1A-94DF-FE8C225BDD9D', 1, 'System', 0)

	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'SupportSignOn_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	COMMIT TRAN
END