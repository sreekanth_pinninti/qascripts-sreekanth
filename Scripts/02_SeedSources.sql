Declare @Name varchar(1000)

Set @Name = 'Core'

begin tran
if not exists (select * from [dbo].Sources with (updlock,serializable) where Name = @Name)
begin
   insert into Sources (InsertAt, UpdateAt, Name)
   values (GetDate(), GetDate(), @Name)
end
commit tran