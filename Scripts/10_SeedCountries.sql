--Seed Country Information

Declare @Name varchar(1000),
		@CountryCode varchar(4),
		@Alpha3Code varchar(4),
		@NumericCode varchar(4),
		@CurrencyCode varchar(1000)

Set @Name = 'Afghanistan'
Set @CountryCode  = 'AF'
Set @Alpha3Code = 'AFG'
Set @NumericCode = '004'
Set @CurrencyCode = 'AFN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Albania'
Set @CountryCode  = 'AL'
Set @Alpha3Code = 'ALB'
Set @NumericCode = '008'
Set @CurrencyCode = 'ALL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Germany'
Set @CountryCode  = 'DE'
Set @Alpha3Code = 'DEU'
Set @NumericCode = '276'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Andorra'
Set @CountryCode  = 'AD'
Set @Alpha3Code = 'AND'
Set @NumericCode = '020'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Angola'
Set @CountryCode  = 'AO'
Set @Alpha3Code = 'AGO'
Set @NumericCode = '024'
Set @CurrencyCode = 'AOA'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Anguilla'
Set @CountryCode  = 'AI'
Set @Alpha3Code = 'AIA'
Set @NumericCode = '660'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Antigua and Barbuda'
Set @CountryCode  = 'AG'
Set @Alpha3Code = 'ATG'
Set @NumericCode = '028'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Antarctica'
Set @CountryCode  = 'AQ'
Set @Alpha3Code = 'ATA'
Set @NumericCode = '010'
Set @CurrencyCode = ''

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saudi Arabia'
Set @CountryCode  = 'SA'
Set @Alpha3Code = 'SAU'
Set @NumericCode = '682'
Set @CurrencyCode = 'SAR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Algeria'
Set @CountryCode  = 'DZ'
Set @Alpha3Code = 'DZA'
Set @NumericCode = '012'
Set @CurrencyCode = 'DZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Argentina'
Set @CountryCode  = 'AR'
Set @Alpha3Code = 'ARG'
Set @NumericCode = '032'
Set @CurrencyCode = 'ARS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Armenia'
Set @CountryCode  = 'AM'
Set @Alpha3Code = 'ARM'
Set @NumericCode = '051'
Set @CurrencyCode = 'AMD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Aruba'
Set @CountryCode  = 'AW'
Set @Alpha3Code = 'ABW'
Set @NumericCode = '533'
Set @CurrencyCode = 'AWG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Australia'
Set @CountryCode  = 'AU'
Set @Alpha3Code = 'AUS'
Set @NumericCode = '036'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Austria'
Set @CountryCode  = 'AT'
Set @Alpha3Code = 'AUT'
Set @NumericCode = '040'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Azerbaijan'
Set @CountryCode  = 'AZ'
Set @Alpha3Code = 'AZE'
Set @NumericCode = '031'
Set @CurrencyCode = 'AZN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bahamas'
Set @CountryCode  = 'BS'
Set @Alpha3Code = 'BHS'
Set @NumericCode = '044'
Set @CurrencyCode = 'BSD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bahrain'
Set @CountryCode  = 'BH'
Set @Alpha3Code = 'BHR'
Set @NumericCode = '048'
Set @CurrencyCode = 'BHD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bangladesh'
Set @CountryCode  = 'BD'
Set @Alpha3Code = 'BGD'
Set @NumericCode = '050'
Set @CurrencyCode = 'BDT'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Barbados'
Set @CountryCode  = 'BB'
Set @Alpha3Code = 'BRB'
Set @NumericCode = '052'
Set @CurrencyCode = 'BBD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Belize'
Set @CountryCode  = 'BZ'
Set @Alpha3Code = 'BLZ'
Set @NumericCode = '084'
Set @CurrencyCode = 'BZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Benin'
Set @CountryCode  = 'BJ'
Set @Alpha3Code = 'BEN'
Set @NumericCode = '204'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bermuda'
Set @CountryCode  = 'BM'
Set @Alpha3Code = 'BMU'
Set @NumericCode = '060'
Set @CurrencyCode = 'BMD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Belarus'
Set @CountryCode  = 'BY'
Set @Alpha3Code = 'BLR'
Set @NumericCode = '112'
Set @CurrencyCode = 'BYN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bolivia'
Set @CountryCode  = 'BO'
Set @Alpha3Code = 'BOL'
Set @NumericCode = '068'
Set @CurrencyCode = 'BOB'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bonaire'
Set @CountryCode  = 'BQ'
Set @Alpha3Code = 'BES'
Set @NumericCode = '535'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bosnia and Herzegovina'
Set @CountryCode  = 'BA'
Set @Alpha3Code = 'BIH'
Set @NumericCode = '070'
Set @CurrencyCode = 'BAM'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Botswana'
Set @CountryCode  = 'BW'
Set @Alpha3Code = 'BWA'
Set @NumericCode = '072'
Set @CurrencyCode = 'BWP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Brazil'
Set @CountryCode  = 'BR'
Set @Alpha3Code = 'BRA'
Set @NumericCode = '076'
Set @CurrencyCode = 'BRL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Brunei'
Set @CountryCode  = 'BN'
Set @Alpha3Code = 'BRN'
Set @NumericCode = '096'
Set @CurrencyCode = 'BND'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bulgaria'
Set @CountryCode  = 'BG'
Set @Alpha3Code = 'BGR'
Set @NumericCode = '100'
Set @CurrencyCode = 'BGN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Burkina Faso'
Set @CountryCode  = 'BF'
Set @Alpha3Code = 'BFA'
Set @NumericCode = '854'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Burundi'
Set @CountryCode  = 'BI'
Set @Alpha3Code = 'BDI'
Set @NumericCode = '108'
Set @CurrencyCode = 'BIF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bhutan'
Set @CountryCode  = 'BT'
Set @Alpha3Code = 'BTN'
Set @NumericCode = '064'
Set @CurrencyCode = 'BTN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Belgium'
Set @CountryCode  = 'BE'
Set @Alpha3Code = 'BEL'
Set @NumericCode = '056'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cabo Verde'
Set @CountryCode  = 'CV'
Set @Alpha3Code = 'CPV'
Set @NumericCode = '132'
Set @CurrencyCode = 'CVE'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cambodia'
Set @CountryCode  = 'KH'
Set @Alpha3Code = 'KHM'
Set @NumericCode = '116'
Set @CurrencyCode = 'KHR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cameroon'
Set @CountryCode  = 'CM'
Set @Alpha3Code = 'CMR'
Set @NumericCode = '120'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Canada'
Set @CountryCode  = 'CA'
Set @Alpha3Code = 'CAN'
Set @NumericCode = '124'
Set @CurrencyCode = 'CAD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Chad'
Set @CountryCode  = 'TD'
Set @Alpha3Code = 'TCD'
Set @NumericCode = '148'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Chile'
Set @CountryCode  = 'CL'
Set @Alpha3Code = 'CHL'
Set @NumericCode = '152'
Set @CurrencyCode = 'CLP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'China'
Set @CountryCode  = 'CN'
Set @Alpha3Code = 'CHN'
Set @NumericCode = '156'
Set @CurrencyCode = 'CNY'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cyprus'
Set @CountryCode  = 'CY'
Set @Alpha3Code = 'CYP'
Set @NumericCode = '196'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Vatican City'
Set @CountryCode  = 'VA'
Set @Alpha3Code = 'VAT'
Set @NumericCode = '336'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Colombia'
Set @CountryCode  = 'CO'
Set @Alpha3Code = 'COL'
Set @NumericCode = '170'
Set @CurrencyCode = 'COP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Comoros'
Set @CountryCode  = 'KM'
Set @Alpha3Code = 'COM'
Set @NumericCode = '174'
Set @CurrencyCode = 'KMF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Republic of the Congo'
Set @CountryCode  = 'CG'
Set @Alpha3Code = 'COG'
Set @NumericCode = '178'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'North Korea'
Set @CountryCode  = 'KP'
Set @Alpha3Code = 'PRK'
Set @NumericCode = '408'
Set @CurrencyCode = 'KPW'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'South Korea'
Set @CountryCode  = 'KR'
Set @Alpha3Code = 'KOR'
Set @NumericCode = '410'
Set @CurrencyCode = 'KRW'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Costa Rica'
Set @CountryCode  = 'CR'
Set @Alpha3Code = 'CRI'
Set @NumericCode = '188'
Set @CurrencyCode = 'CRC'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ivory Coast'
Set @CountryCode  = 'CI'
Set @Alpha3Code = 'CIV'
Set @NumericCode = '384'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Croatia'
Set @CountryCode  = 'HR'
Set @Alpha3Code = 'HRV'
Set @NumericCode = '191'
Set @CurrencyCode = 'HRK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cuba'
Set @CountryCode  = 'CU'
Set @Alpha3Code = 'CUB'
Set @NumericCode = '192'
Set @CurrencyCode = 'CUP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Curacao'
Set @CountryCode  = 'CW'
Set @Alpha3Code = 'CUW'
Set @NumericCode = '531'
Set @CurrencyCode = 'ANG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Denmark'
Set @CountryCode  = 'DK'
Set @Alpha3Code = 'DNK'
Set @NumericCode = '208'
Set @CurrencyCode = 'DKK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Dominica'
Set @CountryCode  = 'DM'
Set @Alpha3Code = 'DMA'
Set @NumericCode = '212'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ecuador'
Set @CountryCode  = 'EC'
Set @Alpha3Code = 'ECU'
Set @NumericCode = '218'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Egypt'
Set @CountryCode  = 'EG'
Set @Alpha3Code = 'EGY'
Set @NumericCode = '818'
Set @CurrencyCode = 'EGP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'El Salvador'
Set @CountryCode  = 'SV'
Set @Alpha3Code = 'SLV'
Set @NumericCode = '222'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'United Arab Emirates'
Set @CountryCode  = 'AE'
Set @Alpha3Code = 'ARE'
Set @NumericCode = '784'
Set @CurrencyCode = 'AED'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Eritrea'
Set @CountryCode  = 'ER'
Set @Alpha3Code = 'ERI'
Set @NumericCode = '232'
Set @CurrencyCode = 'ERN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Slovakia'
Set @CountryCode  = 'SK'
Set @Alpha3Code = 'SVK'
Set @NumericCode = '703'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Slovenia'
Set @CountryCode  = 'SI'
Set @Alpha3Code = 'SVN'
Set @NumericCode = '705'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Spain'
Set @CountryCode  = 'ES'
Set @Alpha3Code = 'ESP'
Set @NumericCode = '724'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'United States'
Set @CountryCode  = 'US'
Set @Alpha3Code = 'USA'
Set @NumericCode = '840'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Estonia'
Set @CountryCode  = 'EE'
Set @Alpha3Code = 'EST'
Set @NumericCode = '233'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ethiopia'
Set @CountryCode  = 'ET'
Set @Alpha3Code = 'ETH'
Set @NumericCode = '231'
Set @CurrencyCode = 'ETB'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Philippines'
Set @CountryCode  = 'PH'
Set @Alpha3Code = 'PHL'
Set @NumericCode = '608'
Set @CurrencyCode = 'PHP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Finland'
Set @CountryCode  = 'FI'
Set @Alpha3Code = 'FIN'
Set @NumericCode = '246'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Fiji'
Set @CountryCode  = 'FJ'
Set @Alpha3Code = 'FJI'
Set @NumericCode = '242'
Set @CurrencyCode = 'FJD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'France'
Set @CountryCode  = 'FR'
Set @Alpha3Code = 'FRA'
Set @NumericCode = '250'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Gabon'
Set @CountryCode  = 'GA'
Set @Alpha3Code = 'GAB'
Set @NumericCode = '266'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Gambia'
Set @CountryCode  = 'GM'
Set @Alpha3Code = 'GMB'
Set @NumericCode = '270'
Set @CurrencyCode = 'GMD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Georgia'
Set @CountryCode  = 'GE'
Set @Alpha3Code = 'GEO'
Set @NumericCode = '268'
Set @CurrencyCode = 'GEL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ghana'
Set @CountryCode  = 'GH'
Set @Alpha3Code = 'GHA'
Set @NumericCode = '288'
Set @CurrencyCode = 'GHS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Gibraltar'
Set @CountryCode  = 'GI'
Set @Alpha3Code = 'GIB'
Set @NumericCode = '292'
Set @CurrencyCode = 'GIP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Grenada'
Set @CountryCode  = 'GD'
Set @Alpha3Code = 'GRD'
Set @NumericCode = '308'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Greece'
Set @CountryCode  = 'GR'
Set @Alpha3Code = 'GRC'
Set @NumericCode = '300'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Greenland'
Set @CountryCode  = 'GL'
Set @Alpha3Code = 'GRL'
Set @NumericCode = '304'
Set @CurrencyCode = 'DKK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guadeloupe'
Set @CountryCode  = 'GP'
Set @Alpha3Code = 'GLP'
Set @NumericCode = '312'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guam'
Set @CountryCode  = 'GU'
Set @Alpha3Code = 'GUM'
Set @NumericCode = '316'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guatemala'
Set @CountryCode  = 'GT'
Set @Alpha3Code = 'GTM'
Set @NumericCode = '320'
Set @CurrencyCode = 'GTQ'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'French Guiana'
Set @CountryCode  = 'GF'
Set @Alpha3Code = 'GUF'
Set @NumericCode = '254'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guernsey'
Set @CountryCode  = 'GG'
Set @Alpha3Code = 'GGY'
Set @NumericCode = '831'
Set @CurrencyCode = 'GBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guinea'
Set @CountryCode  = 'GN'
Set @Alpha3Code = 'GIN'
Set @NumericCode = '324'
Set @CurrencyCode = 'GNF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Equatorial Guinea'
Set @CountryCode  = 'GQ'
Set @Alpha3Code = 'GNQ'
Set @NumericCode = '226'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guinea-Bissau'
Set @CountryCode  = 'GW'
Set @Alpha3Code = 'GNB'
Set @NumericCode = '624'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Guyana'
Set @CountryCode  = 'GY'
Set @Alpha3Code = 'GUY'
Set @NumericCode = '328'
Set @CurrencyCode = 'GYD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Haiti'
Set @CountryCode  = 'HT'
Set @Alpha3Code = 'HTI'
Set @NumericCode = '332'
Set @CurrencyCode = 'HTG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Netherlands'
Set @CountryCode  = 'NL'
Set @Alpha3Code = 'NLD'
Set @NumericCode = '528'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Honduras'
Set @CountryCode  = 'HN'
Set @Alpha3Code = 'HND'
Set @NumericCode = '340'
Set @CurrencyCode = 'HNL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Hong Kong'
Set @CountryCode  = 'HK'
Set @Alpha3Code = 'HKG'
Set @NumericCode = '344'
Set @CurrencyCode = 'HKD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Hungary'
Set @CountryCode  = 'HU'
Set @Alpha3Code = 'HUN'
Set @NumericCode = '348'
Set @CurrencyCode = 'HUF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'India'
Set @CountryCode  = 'IN'
Set @Alpha3Code = 'IND'
Set @NumericCode = '356'
Set @CurrencyCode = 'INR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Indonesia'
Set @CountryCode  = 'ID'
Set @Alpha3Code = 'IDN'
Set @NumericCode = '360'
Set @CurrencyCode = 'IDR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Iraq'
Set @CountryCode  = 'IQ'
Set @Alpha3Code = 'IRQ'
Set @NumericCode = '368'
Set @CurrencyCode = 'IQD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ireland'
Set @CountryCode  = 'IE'
Set @Alpha3Code = 'IRL'
Set @NumericCode = '372'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Iran'
Set @CountryCode  = 'IR'
Set @Alpha3Code = 'IRN'
Set @NumericCode = '364'
Set @CurrencyCode = 'IRR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Bouvet Island'
Set @CountryCode  = 'BV'
Set @Alpha3Code = 'BVT'
Set @NumericCode = '074'
Set @CurrencyCode = 'NOK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Christmas Island'
Set @CountryCode  = 'CX'
Set @Alpha3Code = 'CXR'
Set @NumericCode = '162'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Niue'
Set @CountryCode  = 'NU'
Set @Alpha3Code = 'NIU'
Set @NumericCode = '570'
Set @CurrencyCode = 'NZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Norfolk Island'
Set @CountryCode  = 'NF'
Set @Alpha3Code = 'NFK'
Set @NumericCode = '574'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Isle of Man'
Set @CountryCode  = 'IM'
Set @Alpha3Code = 'IMN'
Set @NumericCode = '833'
Set @CurrencyCode = 'GBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Iceland'
Set @CountryCode  = 'IS'
Set @Alpha3Code = 'ISL'
Set @NumericCode = '352'
Set @CurrencyCode = 'ISK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cayman Islands'
Set @CountryCode  = 'KY'
Set @Alpha3Code = 'CYM'
Set @NumericCode = '136'
Set @CurrencyCode = 'KYD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cocos [Keeling] Islands'
Set @CountryCode  = 'CC'
Set @Alpha3Code = 'CCK'
Set @NumericCode = '166'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Cook Islands'
Set @CountryCode  = 'CK'
Set @Alpha3Code = 'COK'
Set @NumericCode = '184'
Set @CurrencyCode = 'NZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Faroe Islands'
Set @CountryCode  = 'FO'
Set @Alpha3Code = 'FRO'
Set @NumericCode = '234'
Set @CurrencyCode = 'DKK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'South Georgia and the South Sandwich Islands'
Set @CountryCode  = 'GS'
Set @Alpha3Code = 'SGS'
Set @NumericCode = '239'
Set @CurrencyCode = 'GBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Heard Island and McDonald Islands'
Set @CountryCode  = 'HM'
Set @Alpha3Code = 'HMD'
Set @NumericCode = '334'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Falkland Islands'
Set @CountryCode  = 'FK'
Set @Alpha3Code = 'FLK'
Set @NumericCode = '238'
Set @CurrencyCode = 'FKP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Northern Mariana Islands'
Set @CountryCode  = 'MP'
Set @Alpha3Code = 'MNP'
Set @NumericCode = '580'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Marshall Islands'
Set @CountryCode  = 'MH'
Set @Alpha3Code = 'MHL'
Set @NumericCode = '584'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Pitcairn Islands'
Set @CountryCode  = 'PN'
Set @Alpha3Code = 'PCN'
Set @NumericCode = '612'
Set @CurrencyCode = 'NZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Solomon Islands'
Set @CountryCode  = 'SB'
Set @Alpha3Code = 'SLB'
Set @NumericCode = '090'
Set @CurrencyCode = 'SBD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Turks and Caicos Islands'
Set @CountryCode  = 'TC'
Set @Alpha3Code = 'TCA'
Set @NumericCode = '796'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'British Virgin Islands'
Set @CountryCode  = 'VG'
Set @Alpha3Code = 'VGB'
Set @NumericCode = '092'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'U.S. Virgin Islands'
Set @CountryCode  = 'VI'
Set @Alpha3Code = 'VIR'
Set @NumericCode = '850'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Åland'
Set @CountryCode  = 'AX'
Set @Alpha3Code = 'ALA'
Set @NumericCode = '248'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'U.S. Minor Outlying Islands'
Set @CountryCode  = 'UM'
Set @Alpha3Code = 'UMI'
Set @NumericCode = '581'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Israel'
Set @CountryCode  = 'IL'
Set @Alpha3Code = 'ISR'
Set @NumericCode = '376'
Set @CurrencyCode = 'ILS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Italy'
Set @CountryCode  = 'IT'
Set @Alpha3Code = 'ITA'
Set @NumericCode = '380'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Jamaica'
Set @CountryCode  = 'JM'
Set @Alpha3Code = 'JAM'
Set @NumericCode = '388'
Set @CurrencyCode = 'JMD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Japan'
Set @CountryCode  = 'JP'
Set @Alpha3Code = 'JPN'
Set @NumericCode = '392'
Set @CurrencyCode = 'JPY'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Jersey'
Set @CountryCode  = 'JE'
Set @Alpha3Code = 'JEY'
Set @NumericCode = '832'
Set @CurrencyCode = 'GBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Jordan'
Set @CountryCode  = 'JO'
Set @Alpha3Code = 'JOR'
Set @NumericCode = '400'
Set @CurrencyCode = 'JOD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kazakhstan'
Set @CountryCode  = 'KZ'
Set @Alpha3Code = 'KAZ'
Set @NumericCode = '398'
Set @CurrencyCode = 'KZT'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kenya'
Set @CountryCode  = 'KE'
Set @Alpha3Code = 'KEN'
Set @NumericCode = '404'
Set @CurrencyCode = 'KES'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kyrgyzstan'
Set @CountryCode  = 'KG'
Set @Alpha3Code = 'KGZ'
Set @NumericCode = '417'
Set @CurrencyCode = 'KGS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kiribati'
Set @CountryCode  = 'KI'
Set @Alpha3Code = 'KIR'
Set @NumericCode = '296'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kosovo'
Set @CountryCode  = 'XK'
Set @Alpha3Code = 'XKX'
Set @NumericCode = '0'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Kuwait'
Set @CountryCode  = 'KW'
Set @Alpha3Code = 'KWT'
Set @NumericCode = '414'
Set @CurrencyCode = 'KWD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Laos'
Set @CountryCode  = 'LA'
Set @Alpha3Code = 'LAO'
Set @NumericCode = '418'
Set @CurrencyCode = 'LAK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Lesotho'
Set @CountryCode  = 'LS'
Set @Alpha3Code = 'LSO'
Set @NumericCode = '426'
Set @CurrencyCode = 'LSL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Latvia'
Set @CountryCode  = 'LV'
Set @Alpha3Code = 'LVA'
Set @NumericCode = '428'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Liberia'
Set @CountryCode  = 'LR'
Set @Alpha3Code = 'LBR'
Set @NumericCode = '430'
Set @CurrencyCode = 'LRD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Libya'
Set @CountryCode  = 'LY'
Set @Alpha3Code = 'LBY'
Set @NumericCode = '434'
Set @CurrencyCode = 'LYD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Liechtenstein'
Set @CountryCode  = 'LI'
Set @Alpha3Code = 'LIE'
Set @NumericCode = '438'
Set @CurrencyCode = 'CHF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Lithuania'
Set @CountryCode  = 'LT'
Set @Alpha3Code = 'LTU'
Set @NumericCode = '440'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Luxembourg'
Set @CountryCode  = 'LU'
Set @Alpha3Code = 'LUX'
Set @NumericCode = '442'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Lebanon'
Set @CountryCode  = 'LB'
Set @Alpha3Code = 'LBN'
Set @NumericCode = '422'
Set @CurrencyCode = 'LBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Macao'
Set @CountryCode  = 'MO'
Set @Alpha3Code = 'MAC'
Set @NumericCode = '446'
Set @CurrencyCode = 'MOP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'North Macedonia'
Set @CountryCode  = 'MK'
Set @Alpha3Code = 'MKD'
Set @NumericCode = '807'
Set @CurrencyCode = 'MKD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Madagascar'
Set @CountryCode  = 'MG'
Set @Alpha3Code = 'MDG'
Set @NumericCode = '450'
Set @CurrencyCode = 'MGA'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Malaysia'
Set @CountryCode  = 'MY'
Set @Alpha3Code = 'MYS'
Set @NumericCode = '458'
Set @CurrencyCode = 'MYR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Malawi'
Set @CountryCode  = 'MW'
Set @Alpha3Code = 'MWI'
Set @NumericCode = '454'
Set @CurrencyCode = 'MWK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Maldives'
Set @CountryCode  = 'MV'
Set @Alpha3Code = 'MDV'
Set @NumericCode = '462'
Set @CurrencyCode = 'MVR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mali'
Set @CountryCode  = 'ML'
Set @Alpha3Code = 'MLI'
Set @NumericCode = '466'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Malta'
Set @CountryCode  = 'MT'
Set @Alpha3Code = 'MLT'
Set @NumericCode = '470'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Morocco'
Set @CountryCode  = 'MA'
Set @Alpha3Code = 'MAR'
Set @NumericCode = '504'
Set @CurrencyCode = 'MAD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Martinique'
Set @CountryCode  = 'MQ'
Set @Alpha3Code = 'MTQ'
Set @NumericCode = '474'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mauritius'
Set @CountryCode  = 'MU'
Set @Alpha3Code = 'MUS'
Set @NumericCode = '480'
Set @CurrencyCode = 'MUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mauritania'
Set @CountryCode  = 'MR'
Set @Alpha3Code = 'MRT'
Set @NumericCode = '478'
Set @CurrencyCode = 'MRO'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mayotte'
Set @CountryCode  = 'YT'
Set @Alpha3Code = 'MYT'
Set @NumericCode = '175'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Micronesia'
Set @CountryCode  = 'FM'
Set @Alpha3Code = 'FSM'
Set @NumericCode = '583'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Moldova'
Set @CountryCode  = 'MD'
Set @Alpha3Code = 'MDA'
Set @NumericCode = '498'
Set @CurrencyCode = 'MDL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mongolia'
Set @CountryCode  = 'MN'
Set @Alpha3Code = 'MNG'
Set @NumericCode = '496'
Set @CurrencyCode = 'MNT'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Montenegro'
Set @CountryCode  = 'ME'
Set @Alpha3Code = 'MNE'
Set @NumericCode = '499'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Montserrat'
Set @CountryCode  = 'MS'
Set @Alpha3Code = 'MSR'
Set @NumericCode = '500'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mozambique'
Set @CountryCode  = 'MZ'
Set @Alpha3Code = 'MOZ'
Set @NumericCode = '508'
Set @CurrencyCode = 'MZN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Myanmar [Burma]'
Set @CountryCode  = 'MM'
Set @Alpha3Code = 'MMR'
Set @NumericCode = '104'
Set @CurrencyCode = 'MMK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Mexico'
Set @CountryCode  = 'MX'
Set @Alpha3Code = 'MEX'
Set @NumericCode = '484'
Set @CurrencyCode = 'MXN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Monaco'
Set @CountryCode  = 'MC'
Set @Alpha3Code = 'MCO'
Set @NumericCode = '492'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Namibia'
Set @CountryCode  = 'NA'
Set @Alpha3Code = 'NAM'
Set @NumericCode = '516'
Set @CurrencyCode = 'NAD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Nauru'
Set @CountryCode  = 'NR'
Set @Alpha3Code = 'NRU'
Set @NumericCode = '520'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Nepal'
Set @CountryCode  = 'NP'
Set @Alpha3Code = 'NPL'
Set @NumericCode = '524'
Set @CurrencyCode = 'NPR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Nicaragua'
Set @CountryCode  = 'NI'
Set @Alpha3Code = 'NIC'
Set @NumericCode = '558'
Set @CurrencyCode = 'NIO'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Nigeria'
Set @CountryCode  = 'NG'
Set @Alpha3Code = 'NGA'
Set @NumericCode = '566'
Set @CurrencyCode = 'NGN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Norway'
Set @CountryCode  = 'NO'
Set @Alpha3Code = 'NOR'
Set @NumericCode = '578'
Set @CurrencyCode = 'NOK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'New Caledonia'
Set @CountryCode  = 'NC'
Set @Alpha3Code = 'NCL'
Set @NumericCode = '540'
Set @CurrencyCode = 'XPF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'New Zealand'
Set @CountryCode  = 'NZ'
Set @Alpha3Code = 'NZL'
Set @NumericCode = '554'
Set @CurrencyCode = 'NZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Niger'
Set @CountryCode  = 'NE'
Set @Alpha3Code = 'NER'
Set @NumericCode = '562'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Oman'
Set @CountryCode  = 'OM'
Set @Alpha3Code = 'OMN'
Set @NumericCode = '512'
Set @CurrencyCode = 'OMR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Pakistan'
Set @CountryCode  = 'PK'
Set @Alpha3Code = 'PAK'
Set @NumericCode = '586'
Set @CurrencyCode = 'PKR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Palau'
Set @CountryCode  = 'PW'
Set @Alpha3Code = 'PLW'
Set @NumericCode = '585'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Panama'
Set @CountryCode  = 'PA'
Set @Alpha3Code = 'PAN'
Set @NumericCode = '591'
Set @CurrencyCode = 'PAB'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Papua New Guinea'
Set @CountryCode  = 'PG'
Set @Alpha3Code = 'PNG'
Set @NumericCode = '598'
Set @CurrencyCode = 'PGK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Paraguay'
Set @CountryCode  = 'PY'
Set @Alpha3Code = 'PRY'
Set @NumericCode = '600'
Set @CurrencyCode = 'PYG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Peru'
Set @CountryCode  = 'PE'
Set @Alpha3Code = 'PER'
Set @NumericCode = '604'
Set @CurrencyCode = 'PEN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'French Polynesia'
Set @CountryCode  = 'PF'
Set @Alpha3Code = 'PYF'
Set @NumericCode = '258'
Set @CurrencyCode = 'XPF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Poland'
Set @CountryCode  = 'PL'
Set @Alpha3Code = 'POL'
Set @NumericCode = '616'
Set @CurrencyCode = 'PLN'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Portugal'
Set @CountryCode  = 'PT'
Set @Alpha3Code = 'PRT'
Set @NumericCode = '620'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Puerto Rico'
Set @CountryCode  = 'PR'
Set @Alpha3Code = 'PRI'
Set @NumericCode = '630'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Qatar'
Set @CountryCode  = 'QA'
Set @Alpha3Code = 'QAT'
Set @NumericCode = '634'
Set @CurrencyCode = 'QAR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'United Kingdom'
Set @CountryCode  = 'GB'
Set @Alpha3Code = 'GBR'
Set @NumericCode = '826'
Set @CurrencyCode = 'GBP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Central African Republic'
Set @CountryCode  = 'CF'
Set @Alpha3Code = 'CAF'
Set @NumericCode = '140'
Set @CurrencyCode = 'XAF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Czechia'
Set @CountryCode  = 'CZ'
Set @Alpha3Code = 'CZE'
Set @NumericCode = '203'
Set @CurrencyCode = 'CZK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Democratic Republic of the Congo'
Set @CountryCode  = 'CD'
Set @Alpha3Code = 'COD'
Set @NumericCode = '180'
Set @CurrencyCode = 'CDF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Dominican Republic'
Set @CountryCode  = 'DO'
Set @Alpha3Code = 'DOM'
Set @NumericCode = '214'
Set @CurrencyCode = 'DOP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Réunion'
Set @CountryCode  = 'RE'
Set @Alpha3Code = 'REU'
Set @NumericCode = '638'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Rwanda'
Set @CountryCode  = 'RW'
Set @Alpha3Code = 'RWA'
Set @NumericCode = '646'
Set @CurrencyCode = 'RWF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Romania'
Set @CountryCode  = 'RO'
Set @Alpha3Code = 'ROU'
Set @NumericCode = '642'
Set @CurrencyCode = 'RON'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Russia'
Set @CountryCode  = 'RU'
Set @Alpha3Code = 'RUS'
Set @NumericCode = '643'
Set @CurrencyCode = 'RUB'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Samoa'
Set @CountryCode  = 'WS'
Set @Alpha3Code = 'WSM'
Set @NumericCode = '882'
Set @CurrencyCode = 'WST'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'American Samoa'
Set @CountryCode  = 'AS'
Set @Alpha3Code = 'ASM'
Set @NumericCode = '016'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Barthélemy'
Set @CountryCode  = 'BL'
Set @Alpha3Code = 'BLM'
Set @NumericCode = '652'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Kitts and Nevis'
Set @CountryCode  = 'KN'
Set @Alpha3Code = 'KNA'
Set @NumericCode = '659'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'San Marino'
Set @CountryCode  = 'SM'
Set @Alpha3Code = 'SMR'
Set @NumericCode = '674'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Martin'
Set @CountryCode  = 'MF'
Set @Alpha3Code = 'MAF'
Set @NumericCode = '663'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Sint Maarten'
Set @CountryCode  = 'SX'
Set @Alpha3Code = 'SXM'
Set @NumericCode = '534'
Set @CurrencyCode = 'ANG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Pierre and Miquelon'
Set @CountryCode  = 'PM'
Set @Alpha3Code = 'SPM'
Set @NumericCode = '666'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Vincent and the Grenadines'
Set @CountryCode  = 'VC'
Set @Alpha3Code = 'VCT'
Set @NumericCode = '670'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Helena'
Set @CountryCode  = 'SH'
Set @Alpha3Code = 'SHN'
Set @NumericCode = '654'
Set @CurrencyCode = 'SHP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Saint Lucia'
Set @CountryCode  = 'LC'
Set @Alpha3Code = 'LCA'
Set @NumericCode = '662'
Set @CurrencyCode = 'XCD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'São Tomé and Príncipe'
Set @CountryCode  = 'ST'
Set @Alpha3Code = 'STP'
Set @NumericCode = '678'
Set @CurrencyCode = 'STD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Senegal'
Set @CountryCode  = 'SN'
Set @Alpha3Code = 'SEN'
Set @NumericCode = '686'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Serbia'
Set @CountryCode  = 'RS'
Set @Alpha3Code = 'SRB'
Set @NumericCode = '688'
Set @CurrencyCode = 'RSD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Seychelles'
Set @CountryCode  = 'SC'
Set @Alpha3Code = 'SYC'
Set @NumericCode = '690'
Set @CurrencyCode = 'SCR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Sierra Leone'
Set @CountryCode  = 'SL'
Set @Alpha3Code = 'SLE'
Set @NumericCode = '694'
Set @CurrencyCode = 'SLL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Singapore'
Set @CountryCode  = 'SG'
Set @Alpha3Code = 'SGP'
Set @NumericCode = '702'
Set @CurrencyCode = 'SGD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Syria'
Set @CountryCode  = 'SY'
Set @Alpha3Code = 'SYR'
Set @NumericCode = '760'
Set @CurrencyCode = 'SYP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Somalia'
Set @CountryCode  = 'SO'
Set @Alpha3Code = 'SOM'
Set @NumericCode = '706'
Set @CurrencyCode = 'SOS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Sri Lanka'
Set @CountryCode  = 'LK'
Set @Alpha3Code = 'LKA'
Set @NumericCode = '144'
Set @CurrencyCode = 'LKR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Eswatini'
Set @CountryCode  = 'SZ'
Set @Alpha3Code = 'SWZ'
Set @NumericCode = '748'
Set @CurrencyCode = 'SZL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'South Africa'
Set @CountryCode  = 'ZA'
Set @Alpha3Code = 'ZAF'
Set @NumericCode = '710'
Set @CurrencyCode = 'ZAR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Sudan'
Set @CountryCode  = 'SD'
Set @Alpha3Code = 'SDN'
Set @NumericCode = '729'
Set @CurrencyCode = 'SDG'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'South Sudan'
Set @CountryCode  = 'SS'
Set @Alpha3Code = 'SSD'
Set @NumericCode = '728'
Set @CurrencyCode = 'SSP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Sweden'
Set @CountryCode  = 'SE'
Set @Alpha3Code = 'SWE'
Set @NumericCode = '752'
Set @CurrencyCode = 'SEK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Switzerland'
Set @CountryCode  = 'CH'
Set @Alpha3Code = 'CHE'
Set @NumericCode = '756'
Set @CurrencyCode = 'CHF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Suriname'
Set @CountryCode  = 'SR'
Set @Alpha3Code = 'SUR'
Set @NumericCode = '740'
Set @CurrencyCode = 'SRD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Svalbard and Jan Mayen'
Set @CountryCode  = 'SJ'
Set @Alpha3Code = 'SJM'
Set @NumericCode = '744'
Set @CurrencyCode = 'NOK'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Western Sahara'
Set @CountryCode  = 'EH'
Set @Alpha3Code = 'ESH'
Set @NumericCode = '732'
Set @CurrencyCode = 'MAD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Thailand'
Set @CountryCode  = 'TH'
Set @Alpha3Code = 'THA'
Set @NumericCode = '764'
Set @CurrencyCode = 'THB'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Taiwan'
Set @CountryCode  = 'TW'
Set @Alpha3Code = 'TWN'
Set @NumericCode = '158'
Set @CurrencyCode = 'TWD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tanzania'
Set @CountryCode  = 'TZ'
Set @Alpha3Code = 'TZA'
Set @NumericCode = '834'
Set @CurrencyCode = 'TZS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tajikistan'
Set @CountryCode  = 'TJ'
Set @Alpha3Code = 'TJK'
Set @NumericCode = '762'
Set @CurrencyCode = 'TJS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'British Indian Ocean Territory'
Set @CountryCode  = 'IO'
Set @Alpha3Code = 'IOT'
Set @NumericCode = '086'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'French Southern Territories'
Set @CountryCode  = 'TF'
Set @Alpha3Code = 'ATF'
Set @NumericCode = '260'
Set @CurrencyCode = 'EUR'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Palestine'
Set @CountryCode  = 'PS'
Set @Alpha3Code = 'PSE'
Set @NumericCode = '275'
Set @CurrencyCode = 'ILS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Timor-Leste'
Set @CountryCode  = 'TL'
Set @Alpha3Code = 'TLS'
Set @NumericCode = '626'
Set @CurrencyCode = 'USD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Togo'
Set @CountryCode  = 'TG'
Set @Alpha3Code = 'TGO'
Set @NumericCode = '768'
Set @CurrencyCode = 'XOF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tokelau'
Set @CountryCode  = 'TK'
Set @Alpha3Code = 'TKL'
Set @NumericCode = '772'
Set @CurrencyCode = 'NZD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tonga'
Set @CountryCode  = 'TO'
Set @Alpha3Code = 'TON'
Set @NumericCode = '776'
Set @CurrencyCode = 'TOP'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Trinidad and Tobago'
Set @CountryCode  = 'TT'
Set @Alpha3Code = 'TTO'
Set @NumericCode = '780'
Set @CurrencyCode = 'TTD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Turkmenistan'
Set @CountryCode  = 'TM'
Set @Alpha3Code = 'TKM'
Set @NumericCode = '795'
Set @CurrencyCode = 'TMT'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Turkey'
Set @CountryCode  = 'TR'
Set @Alpha3Code = 'TUR'
Set @NumericCode = '792'
Set @CurrencyCode = 'TRY'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tuvalu'
Set @CountryCode  = 'TV'
Set @Alpha3Code = 'TUV'
Set @NumericCode = '798'
Set @CurrencyCode = 'AUD'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Tunisia'
Set @CountryCode  = 'TN'
Set @Alpha3Code = 'TUN'
Set @NumericCode = '788'
Set @CurrencyCode = 'TND'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Ukraine'
Set @CountryCode  = 'UA'
Set @Alpha3Code = 'UKR'
Set @NumericCode = '804'
Set @CurrencyCode = 'UAH'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Uganda'
Set @CountryCode  = 'UG'
Set @Alpha3Code = 'UGA'
Set @NumericCode = '800'
Set @CurrencyCode = 'UGX'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Uruguay'
Set @CountryCode  = 'UY'
Set @Alpha3Code = 'URY'
Set @NumericCode = '858'
Set @CurrencyCode = 'UYU'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Uzbekistan'
Set @CountryCode  = 'UZ'
Set @Alpha3Code = 'UZB'
Set @NumericCode = '860'
Set @CurrencyCode = 'UZS'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Vanuatu'
Set @CountryCode  = 'VU'
Set @Alpha3Code = 'VUT'
Set @NumericCode = '548'
Set @CurrencyCode = 'VUV'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Venezuela'
Set @CountryCode  = 'VE'
Set @Alpha3Code = 'VEN'
Set @NumericCode = '862'
Set @CurrencyCode = 'VES'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Vietnam'
Set @CountryCode  = 'VN'
Set @Alpha3Code = 'VNM'
Set @NumericCode = '704'
Set @CurrencyCode = 'VND'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Wallis and Futuna'
Set @CountryCode  = 'WF'
Set @Alpha3Code = 'WLF'
Set @NumericCode = '876'
Set @CurrencyCode = 'XPF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Yemen'
Set @CountryCode  = 'YE'
Set @Alpha3Code = 'YEM'
Set @NumericCode = '887'
Set @CurrencyCode = 'YER'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Djibouti'
Set @CountryCode  = 'DJ'
Set @Alpha3Code = 'DJI'
Set @NumericCode = '262'
Set @CurrencyCode = 'DJF'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Zambia'
Set @CountryCode  = 'ZM'
Set @Alpha3Code = 'ZMB'
Set @NumericCode = '894'
Set @CurrencyCode = 'ZMW'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran

Set @Name = 'Zimbabwe'
Set @CountryCode  = 'ZW'
Set @Alpha3Code = 'ZWE'
Set @NumericCode = '716'
Set @CurrencyCode = 'ZWL'

begin tran
if not exists (select * from Countries with (updlock,serializable) where CountryCode = @CountryCode)
begin
    insert into Countries (InsertAt, UpdateAt, Name, CountryCode, Alpha3Code, NumericCode, CurrencyCode)
    values (GetDate(), GetDate(), @Name, @CountryCode, @Alpha3Code, @NumericCode, @CurrencyCode)
end
commit tran


